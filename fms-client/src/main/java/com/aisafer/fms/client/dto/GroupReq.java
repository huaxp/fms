package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class GroupReq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3124654384541669564L;
	
	
	/**
	 * 区块大小（单位（b）（4M））
	 */
	private int blockSize = 2097152;
	
	/**
	 * 分片后同时请求并发数
	 */
	private int concurrentNum = 10;
	
	private int timeout = 120;
	
	private int  shardsNum = 10;


	public int getShardsNum() {
		return shardsNum;
	}

	public void setShardsNum(int shardsNum) {
		this.shardsNum = shardsNum;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}

	public int getConcurrentNum() {
		return concurrentNum;
	}

	public void setConcurrentNum(int concurrentNum) {
		this.concurrentNum = concurrentNum;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
}
