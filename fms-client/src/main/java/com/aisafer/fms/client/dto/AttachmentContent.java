package com.aisafer.fms.client.dto;

import java.io.Serializable;

import com.aisafer.fms.client.enums.StorageTypeEnums;

/**
 * oss参数
 */
public class AttachmentContent  implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 6971553951121423168L;
		/**
		 * 文件类型
		 */
		private String fileType;
		/**
		 * 文件大小
		 */
		private Long fileSize;
		
		
		private byte[] content;
		
		/**
		 * 文件名称
		 */
		private String fileName;
		
		private String sourceCode;
		
		private String storageKey;
		
		private StorageTypeEnums storageType;
		

		public String getFileType() {
			return fileType;
		}

		public void setFileType(String fileType) {
			this.fileType = fileType;
		}

		public Long getFileSize() {
			return fileSize;
		}

		public void setFileSize(Long fileSize) {
			this.fileSize = fileSize;
		}


		public String getSourceCode() {
			return sourceCode;
		}

		public void setSourceCode(String sourceCode) {
			this.sourceCode = sourceCode;
		}

		public byte[] getContent() {
			return content;
		}

		public void setContent(byte[] content) {
			this.content = content;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public String getStorageKey() {
			return storageKey;
		}

		public void setStorageKey(String storageKey) {
			this.storageKey = storageKey;
		}

		public StorageTypeEnums getStorageType() {
			return storageType;
		}

		public void setStorageType(StorageTypeEnums storageType) {
			this.storageType = storageType;
		}

		
		
		
}
