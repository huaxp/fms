package com.aisafer.fms.client.enums;

public enum ViewMode {
	VIEW,
	DOWN,
	BYTE,
	BASE64;
}
