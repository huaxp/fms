package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class DownReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 918871965473039696L;

	private Long attachid;
	
	private String modulekey;
	
	private Integer width;
	
	private Integer height;
	
	private String  storageKey;

	public Long getAttachid() {
		return attachid;
	}

	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}

	public String getModulekey() {
		return modulekey;
	}

	public void setModulekey(String modulekey) {
		this.modulekey = modulekey;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getStorageKey() {
		return storageKey;
	}

	public void setStorageKey(String storageKey) {
		this.storageKey = storageKey;
	}
	
	
}
