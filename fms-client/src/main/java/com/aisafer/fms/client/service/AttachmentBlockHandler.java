package com.aisafer.fms.client.service;

import com.aisafer.base.exception.ServiceException;
import com.aisafer.base.pojo.Response;
import com.aisafer.fms.client.dto.AttachmentReq;
import com.aisafer.fms.client.dto.AttachmentResp;
import com.aisafer.fms.client.dto.DownGroupReq;
import com.aisafer.fms.client.dto.DownResp;
import com.aisafer.fms.client.dto.GroupReq;

public interface AttachmentBlockHandler {
	
	Response<AttachmentResp> uploadFile(AttachmentReq req,GroupReq groupReq);
	
	
	/**
	 * 获取云端附件。
	 * @param attachId   存在在oss库中的附件id。
	 * @return
	 */
	byte[]  downloadFileByte(DownGroupReq req) throws  ServiceException;
	
	/**
	 * 获取云端附件。
	 * @param attachId   存在在oss库中的附件id。
	 * @return
	 */
	DownResp  download(DownGroupReq req) throws  ServiceException;
	
	
}
