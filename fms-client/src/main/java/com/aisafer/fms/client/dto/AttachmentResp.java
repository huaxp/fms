package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class AttachmentResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3575440299035104079L;
	
	/**
	 * 附件id
	 */
	private Long attachid;
	
	
	private String storageKey;
	
	private Integer size;
	
	private String fileType;
	

	public String getStorageKey() {
		return storageKey;
	}

	public void setStorageKey(String storageKey) {
		this.storageKey = storageKey;
	}

	public Long getAttachid() {
		return attachid;
	}

	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
}
