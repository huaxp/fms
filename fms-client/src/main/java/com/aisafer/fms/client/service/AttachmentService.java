package com.aisafer.fms.client.service;

import java.util.List;
import java.util.Map;

import com.aisafer.base.exception.ServiceException;
import com.aisafer.base.pojo.Response;
import com.aisafer.fms.client.dto.AttachmentAdditionalReq;
import com.aisafer.fms.client.dto.AttachmentModifyReq;
import com.aisafer.fms.client.dto.AttachmentModifyResp;
import com.aisafer.fms.client.dto.AttachmentReq;
import com.aisafer.fms.client.dto.AttachmentResp;
import com.aisafer.fms.client.dto.AuthReq;
import com.aisafer.fms.client.dto.DownReq;
import com.aisafer.fms.client.dto.DownResp;

/**
 * oss服务接口
 */
public interface AttachmentService {
	
	/**
	 * 上传文件到云端。<br>
	 * 存在两种场景：<br>
	 * 1，通过oss服务器正常上传文件<br>
	 * 2，通过oss以外服务器上传文件，但是需要从oss服务下载文件，此种情况需要先把云端附件信息存放到oss库中，以便可以下在OSS服务器下载。<br>
	 * @param param
	 * @return
	 * @throws n 
	 */
	Response<AttachmentResp> uploadFile(AttachmentReq req) throws  ServiceException;
	
	
	Response<AttachmentResp> uploadAdditionalFile(AttachmentAdditionalReq req) throws  ServiceException;
	
	Response<AttachmentResp> uploadRollbackAdditionalFile(String  batchid) throws  ServiceException;
	
	Response<AttachmentResp> uploadMergeFile(AttachmentAdditionalReq attachReq) throws  ServiceException;
	
	
	/**
	 * 获取云端附件。
	 * @param attachId   存在在oss库中的附件id。
	 * @return
	 */
	byte[]  downloadFileByte(DownReq req) throws  ServiceException;
	
	/**
	 * 获取云端附件。
	 * @param attachId   存在在oss库中的附件id。
	 * @return
	 */
	DownResp  download(DownReq req) throws  ServiceException;
	
	
	public Response<AttachmentModifyResp> attachmentModify(AttachmentModifyReq req);


	Response<String> getAuthUrl(AuthReq req) ;
	
	public Map<Long,AttachmentResp> getAttachment(List<Long> list);
	
	
}
