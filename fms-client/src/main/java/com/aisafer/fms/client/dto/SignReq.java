package com.aisafer.fms.client.dto;

import java.io.Serializable;
import java.util.Date;

public class SignReq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2723272435868364327L;

	private Date expirationDate;
	
	private String cloudKey;
	
	private String appId;
	
	private String attachid;
	

	public String getAttachid() {
		return attachid;
	}

	public void setAttachid(String attachid) {
		this.attachid = attachid;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCloudKey() {
		return cloudKey;
	}

	public void setCloudKey(String cloudKey) {
		this.cloudKey = cloudKey;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	
}
