package com.aisafer.fms.client.dto;
import java.io.Serializable;
import java.util.Date;

public class ModuleResp implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 375374021355833482L;
	private String id;
	/**模块键**/
	private String moduleKey;
	private String isEnable;
	private String moduleDesc;
	private Date createTime;
	private Date updateTime;
	private String secretKey;
	
	
	public ModuleResp(String moduleKey) {
		this.moduleKey = moduleKey;
	}
	public ModuleResp() {
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getModuleKey() {
		return moduleKey;
	}
	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}
	public String getIsEnable() {
		return isEnable;
	}
	public void setIsEnable(String isEnable) {
		this.isEnable = isEnable;
	}
	public String getModuleDesc() {
		return moduleDesc;
	}
	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	
}

