package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class DownGroupReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3909746696656923219L;

	
	private Long attachid;
	
	private Integer timeout = 60;

	private Integer concurrentNum = 10;


	public Long getAttachid() {
		return attachid;
	}

	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public Integer getConcurrentNum() {
		return concurrentNum;
	}

	public void setConcurrentNum(Integer concurrentNum) {
		this.concurrentNum = concurrentNum;
	}
}
