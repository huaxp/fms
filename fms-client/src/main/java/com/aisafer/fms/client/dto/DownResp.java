package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class DownResp implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5818938637838288675L;
	private String fileName;
	private String fileType;
	private byte[] content;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	
}
