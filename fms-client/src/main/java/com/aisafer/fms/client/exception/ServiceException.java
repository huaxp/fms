package com.aisafer.fms.client.exception;

public class ServiceException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 3338945095699568333L;

	public ServiceException(String message) {
		super(message);
	}

	
}
