package com.aisafer.fms.client.service;

import java.util.List;

import com.aisafer.base.pojo.Response;
import com.aisafer.fms.client.dto.AttachmentGroupReq;
import com.aisafer.fms.client.dto.AttachmentGroupResp;

public interface AttachmentGroupService {
	
	Response<AttachmentGroupResp> insertGroup(List<AttachmentGroupReq> req);
	
	List<AttachmentGroupResp> getList(AttachmentGroupReq req);
	
}
