package com.aisafer.fms.client.dto;

import java.io.Serializable;

/**
 * oss参数
 */
public class AttachmentAdditionalReq extends AttachmentReq  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5757361697956126737L;
	
	private AttachmentAdditionalContent attachmentAdditionalContent;

	public AttachmentAdditionalContent getAttachmentAdditionalContent() {
		return attachmentAdditionalContent;
	}

	public void setAttachmentAdditionalContent(AttachmentAdditionalContent attachmentAdditionalContent) {
		this.attachmentAdditionalContent = attachmentAdditionalContent;
	}

}
