package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class AttachmentModifyReq implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6075457692263707522L;

	/**
	 * 分类类型（如：身份证，银行卡等）
	 */
	private String categoryType;
	
	/**
	 * 来源编号
	 */
	private String sourceCode;
	
	/**
	 * 系统来源
	 */
	private String sourceSystem;
	
	private Long attachid;
	

	public Long getAttachid() {
		return attachid;
	}

	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	
}
