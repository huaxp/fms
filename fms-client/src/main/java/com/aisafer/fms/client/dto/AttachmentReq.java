package com.aisafer.fms.client.dto;

import java.io.Serializable;

/**
 * oss参数
 */
public class AttachmentReq  implements Serializable{
	/**
	 * 
	 */
		private static final long serialVersionUID = -5757361697956126737L;

		/**
		 * OSS生成健
		 */
		private String moduleKey;
		
		/**
		 * 文件内容
		 */
		
		private AttachmentContent attachmentContent;
		
		
		private boolean isEncrypt;
		

		public String getModuleKey() {
			return moduleKey;
		}


		public void setModuleKey(String moduleKey) {
			this.moduleKey = moduleKey;
		}



		public boolean isEncrypt() {
			return isEncrypt;
		}


		public void setEncrypt(boolean isEncrypt) {
			this.isEncrypt = isEncrypt;
		}


		public AttachmentContent getAttachmentContent() {
			return attachmentContent;
		}


		public void setAttachmentContent(AttachmentContent attachmentContent) {
			this.attachmentContent = attachmentContent;
		}
		

}
