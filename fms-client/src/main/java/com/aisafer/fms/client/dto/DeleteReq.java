package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class DeleteReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 918871965473039696L;

	private Long attachid;
	
	private String StorageKey;
	
	
	public Long getAttachid() {
		return attachid;
	}


	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}


	public String getStorageKey() {
		return StorageKey;
	}


	public void setStorageKey(String storageKey) {
		StorageKey = storageKey;
	}
	
	
}
