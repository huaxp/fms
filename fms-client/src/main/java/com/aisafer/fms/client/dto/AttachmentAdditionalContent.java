package com.aisafer.fms.client.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * oss参数
 */
public class AttachmentAdditionalContent extends AttachmentContent  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6971553951121423168L;
		
	private String batchid;
	
	
	private Order order;


	public String getBatchid() {
		return batchid;
	}


	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}


	public Order getOrder() {
		return order;
	}


	public void setOrder(Order order) {
		this.order = order;
	}

	
}
