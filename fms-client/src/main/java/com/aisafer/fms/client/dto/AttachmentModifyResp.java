package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class AttachmentModifyResp implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6075457692263707522L;

	private Long attachid;

	public Long getAttachid() {
		return attachid;
	}

	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}
	

	
}
