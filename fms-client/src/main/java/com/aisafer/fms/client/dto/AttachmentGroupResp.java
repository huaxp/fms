package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class AttachmentGroupResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3575440299035104079L;
	
	private Long attachid;
	private Long groupid;
	private int fileBlock;
	public Long getAttachid() {
		return attachid;
	}
	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}
	public Long getGroupid() {
		return groupid;
	}
	public void setGroupid(Long groupid) {
		this.groupid = groupid;
	}
	public int getFileBlock() {
		return fileBlock;
	}
	public void setFileBlock(int fileBlock) {
		this.fileBlock = fileBlock;
	}
	
	
}
