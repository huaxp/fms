package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class Order implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 301197568930990374L;
	private long attachid;
	private int order;
	private int size;
	private String storageKey;
	
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public long getAttachid() {
		return attachid;
	}
	public void setAttachid(long attachid) {
		this.attachid = attachid;
	}
	public String getStorageKey() {
		return storageKey;
	}
	public void setStorageKey(String storageKey) {
		this.storageKey = storageKey;
	}
	
	
}
