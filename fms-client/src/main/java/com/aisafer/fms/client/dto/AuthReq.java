package com.aisafer.fms.client.dto;

import java.io.Serializable;

import com.aisafer.fms.client.enums.ViewMode;

public class AuthReq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2723272435868364327L;

	private Long urlExpire;
	
	private Long attachid;
	
	private String modulekey;
	
	private String urlType;
	
	private Integer height;
	
	private Integer width;
	
	private ViewMode viewMode;
	

	public Long getUrlExpire() {
		return urlExpire;
	}

	public void setUrlExpire(Long urlExpire) {
		this.urlExpire = urlExpire;
	}

	public Long getAttachid() {
		return attachid;
	}

	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}


	public String getModulekey() {
		return modulekey;
	}

	public void setModulekey(String modulekey) {
		this.modulekey = modulekey;
	}

	public String getUrlType() {
		return urlType;
	}

	public void setUrlType(String urlType) {
		this.urlType = urlType;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public ViewMode getViewMode() {
		return viewMode;
	}

	public void setViewMode(ViewMode viewMode) {
		this.viewMode = viewMode;
	}

	
	
}
