package com.aisafer.fms.client.dto;
import java.io.Serializable;

public class ModuleReq implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 375374021355833482L;
	private String moduleKey;
	private String isEnable;
	
	public ModuleReq(String moduleKey) {
		this.moduleKey = moduleKey;
	}
	public ModuleReq() {
	}
	public String getModuleKey() {
		return moduleKey;
	}
	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}
	public String getIsEnable() {
		return isEnable;
	}
	public void setIsEnable(String isEnable) {
		this.isEnable = isEnable;
	}
	
	
}

