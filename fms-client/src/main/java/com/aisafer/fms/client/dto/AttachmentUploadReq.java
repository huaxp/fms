package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class AttachmentUploadReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6130201427495222097L;

	
	private String moduleKey;
	private String sourceCode;
	private String sourceSystem;
	
	/**
	 * url过期时间（秒）
	 */
	private long urlExpire;
	
	/**
	 * 文件类型
	 */
	private String fileType;
	/**
	 * 文件大小
	 */
	private Long fileSize;
	
	
	/**
	 * 分类类型（如：身份证，银行卡等）
	 */
	private String categoryType;
	
	/**
	 * 存储放在云端健
	 */
	private String storageKey;
	
	/**
	 * 文件名称
	 */
	private String fileName;

	public String getModuleKey() {
		return moduleKey;
	}

	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public long getUrlExpire() {
		return urlExpire;
	}

	public void setUrlExpire(long urlExpire) {
		this.urlExpire = urlExpire;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getStorageKey() {
		return storageKey;
	}

	public void setStorageKey(String storageKey) {
		this.storageKey = storageKey;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	
}
