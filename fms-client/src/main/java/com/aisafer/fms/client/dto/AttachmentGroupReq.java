package com.aisafer.fms.client.dto;

import java.io.Serializable;

public class AttachmentGroupReq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -649733853017241656L;

	
	private Long attachid;

    private Long groupid;

    private Integer fileBlock;



	public Long getAttachid() {
		return attachid;
	}

	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}

	public Long getGroupid() {
		return groupid;
	}

	public void setGroupid(Long groupid) {
		this.groupid = groupid;
	}

	public Integer getFileBlock() {
		return fileBlock;
	}

	public void setFileBlock(Integer fileBlock) {
		this.fileBlock = fileBlock;
	}


	
}
