package com.aisafer.fms.consumer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aisafer.fms.domain.OssMsg;
import com.aisafer.fms.service.impl.AttachmentRoute;
import com.easy.mq.annotation.KafkaListener;
import com.easy.mq.event.MQEvent;

/**
 * 消息消费接收端
 * @author stevin
 */
@Component
public class OSSConsumerMq {

	@Autowired
	AttachmentRoute route;
	
	@KafkaListener(topic = "$topics[oss]",group="$group.id[oss]")
	public void consumerHandler(List<MQEvent<OssMsg>> message) {
		List<OssMsg>  ossList = new ArrayList<>();
		for(MQEvent<OssMsg> msg : message) {
			ossList.add(msg.getContent());
		}
		route.broadcast(ossList);
	}
	
}
