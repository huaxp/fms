package com.aisafer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;


@MapperScan("com.aisafer.fms.mapper")
@ImportResource(value={"classpath:/config/*.xml"})
@SpringBootApplication
public class JobApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(JobApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(JobApplication.class, args);
	}

}