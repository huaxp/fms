package com.aisafer.oss;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.ParseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.aisafer.aisaferJobApplication;
import com.aisafer.base.file.FileTypeJudge;
import com.aisafer.base.file.FileUtils;
import com.aisafer.base.pojo.Response;
import com.aisafer.base.utils.JSONUtil;
import com.aisafer.open.api.SmjrApiHelper;
import com.aisafer.open.api.crypt.Base64;
import com.aisafer.open.api.dto.HttpParam;
import com.aisafer.open.api.dto.ResponseContent;
import com.aisafer.oss.dro.AttachmentContent;
import com.aisafer.oss.dro.AttachmentReq;
import com.aisafer.oss.dro.AuthReq;
import com.aisafer.oss.dro.DownReq;
import com.aisafer.oss.dto.AttachmentResp;
import com.aisafer.oss.dto.DownResp;
import com.aisafer.oss.service.OSSService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = aisaferJobApplication.class)
public class OSSServiceTest {

	@Autowired
	OSSService service;

	String path = "d:/doc/互联网研发管理 V1.0(2).pdf";
	String key = "";
	String storagePath = "d:/file/";

	@Test
	public void uploadFileTest() {

		AttachmentReq attReq = new AttachmentReq();
		attReq.setModuleKey("CFBS");
		attReq.setSourceCode("1000000000000000000000000001");
		attReq.setUrlExpire(3600);
		AttachmentContent attachmentContent = null;

		attachmentContent = new AttachmentContent();
		attachmentContent.setContent(FileUtils.getFile().getFilebyte(path));
		attachmentContent.setFileSize(Integer.valueOf(attachmentContent.getContent().length).longValue());
		attachmentContent.setFileType("pdf");
		attachmentContent.setFileName("tem.pdf");
		attReq.addContent(attachmentContent);
		Response<AttachmentResp> resp = service.uploadFile(attReq);
		System.out.println(JSONUtil.toJson(resp));
	}

	@Test
	public void downloadFileTest() throws IOException {
		DownReq req = new DownReq();
		// req.setAttachId("5b9f1652ddd74eeb9418fd30bc06c105");
		req.setStorageKey("af94e5f73a9441d9ba269502d7c3ce0a");
		req.setModulekey("CFBS");
		long begin = System.currentTimeMillis();
		byte[] buf = service.downloadFileByte(req);
		System.out.println("length:" + buf.length);
		FileUtils.getFile().byte2File(buf, storagePath, "100002.pdf");
		System.out.println(FileTypeJudge.getType(buf));
		System.out.println("time:" + (System.currentTimeMillis() - begin));
	}

	@Test
	public void reqAuthInfo() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("md5Key", "CFBS");
		SmjrApiHelper helper = new SmjrApiHelper("CFBS", "tS5awNlr2O0odAWkgOad6g==", "CFBS");
		ResponseContent respon = helper.sendRequest("http://120.55.163.223:9876/oss/api/entry?", "smjr.oss.authinfo",
				content);
		System.out.println(JSONUtil.toJson(respon));
	}

	@Test
	public void getAuthInfo() {
		/*
		 * Response<String> response = service.getAuthInfo("OSSTEST");
		 * JSONUtil.toJson(response);
		 */
	}

	@Test
	public void getAuthUrl() {
		AuthReq req = new AuthReq();
		req.setModulekey("RISK");
		req.setAttachid("a39ac268f4c84a3e94740a156c648201");
		Response<String> response = service.getAuthUrl(req);
		JSONUtil.toJson(response);
	}

	public void uploadRemoteTest() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("md5Key", "RISK");
		// content.put("file",
		// Base64.encode(FileUtils.getFile().getFilebyte("d:/doc/2.png")));
		// content.put("file", Base64.encode("hello".getBytes()));
		content.put("file", Base64.encode(FileUtils.getFile().getFilebyte("/home/stevin/123.docx")));
		content.put("fileName", "11.docx");
		content.put("fileType", ".docx");
		content.put("sourceCode", "1005");
		content.put("urlExpire", "3600");// url访问过期时间
		SmjrApiHelper helper = new SmjrApiHelper("RISK", "tS5awNlr2O0odAWkgOad6g==", "RISK");
		HttpParam httpParam = new HttpParam();
		String respon = helper.sendRequest("http://120.55.163.223:9876/oss/api/entry?", "smjr.oss.upload", content,
				httpParam);
		// String respon = helper.sendRequest("http://localhost:8081/api/entry?",
		// "smjr.oss.upload", content,httpParam);
		// String respon =
		// helper.sendRequest("http://192.168.14.26:9876/oss/api/entry?",
		// "smjr.oss.upload", content,httpParam);
		System.out.println(JSONUtil.toJson(respon));
	}

	public void uploadRemoteTest2() throws ParseException, IOException {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("md5Key", "CFBS");
		content.put("sourceCode", "1010");
		content.put("fileType", ".pdf");
		content.put("urlExpire", "3600");// url访问过期时间
		content.put("fileName", "1001.pdf");
		SmjrApiHelper helper = new SmjrApiHelper("CFBS", "tS5awNlr2O0odAWkgOad6g==", "CFBS", "2.0");
		HttpParam httpParam = new HttpParam();
		// String respon =
		// helper.sendRequest("http://120.55.163.223:9876/oss/api/entry?",
		// "smjr.oss.upload", content,httpParam,FileUtils.getFile().getFilebyte(path));
/*		String respon = helper.sendRequest("http://139.224.8.8:9876/oss/api/entry?", "smjr.oss.upload", content,
				httpParam, FileUtils.getFile().getFilebyte(path));*/
		String respon = helper.sendRequest("http://192.169.10.21:9876/aisafer-oss-provider/api/entry?", "smjr.oss.upload", content,
				httpParam, FileUtils.getFile().getFilebyte(path));
		// String respon = helper.sendRequest("http://192.168.10.29:8084/api/entry?",
		// "smjr.oss.upload", content,httpParam,FileUtils.getFile().getFilebyte(path));
		// String respon = helper.sendRequest("http://106.75.101.221:80/oss/api/entry?",
		// "smjr.oss.upload", content,httpParam,FileUtils.getFile().getFilebyte(path));
		// String respon = helper.sendRequest("http://127.0.0.1:8083/api/entry?",
		// "smjr.oss.upload", content,httpParam,FileUtils.getFile().getFilebyte(path));
		// String respon =
		// helper.sendRequest("http://oss-api.aisaferjr.com/oss/api/entry?",
		// "smjr.oss.upload", content,httpParam,FileUtils.getFile().getFilebyte(path));
		System.out.println(JSONUtil.toJson(respon));
	}

	public void authurl() throws ParseException, IOException {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("md5Key", "CFBS");
		content.put("attachid", "5972190c55cf4be38652ee9d7533505a");
		content.put("expire", new Date(System.currentTimeMillis() + 3600 * 1000).getTime());
		SmjrApiHelper helper = new SmjrApiHelper("CFBS", "tS5awNlr2O0odAWkgOad6g==", "CFBS");
		HttpParam httpParam = new HttpParam();

		// String respon =
		// helper.sendRequest("http://120.55.163.223:9876/oss/api/entry?",
		// "smjr.oss.upload",
		// content,httpParam,FileUtils.getFile().getFilebyte("/home/stevin/tem.pdf"));
		String respon = helper.sendRequest("http://139.224.8.8:9876/oss/api/entry?", "smjr.oss.upload", content,
				httpParam, FileUtils.getFile().getFilebyte("/home/stevin/tem.pdf"));
		// String respon = helper.sendRequest("http://localhost:8084/api/entry?",
		// "smjr.oss.authurl", content,httpParam);
		// String respon = helper.sendRequest("http://106.75.101.221:80/oss/api/entry?",
		// "smjr.oss.upload",
		// content,httpParam,FileUtils.getFile().getFilebyte("/home/stevin/tem.pdf"));
		// String respon =
		// helper.sendRequest("http://192.168.14.26:9876/oss/api/entry?",
		// "smjr.oss.upload", content,httpParam);
		// String respon =
		// helper.sendRequest("http://oss-api.aisaferjr.com/oss/api/entry?",
		// "smjr.oss.upload",
		// content,httpParam,FileUtils.getFile().getFilebyte("/home/stevin/tem.pdf"));
		System.out.println(JSONUtil.toJson(respon));
	}

	public void uploadTest() throws ParseException, IOException {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("md5Key", "RISK");
		SmjrApiHelper helper = new SmjrApiHelper("RISK", "tS5awNlr2O0odAWkgOad6g==", "RISK");
		HttpParam httpParam = new HttpParam();

		// String respon =
		// helper.sendRequest("http://120.55.163.223:9876/oss/api/entry?",
		// "smjr.oss.upload",
		// content,httpParam,FileUtils.getFile().getFilebyte("/home/stevin/software/lib/mockito-core-1.10.19.jar"));
		String respon = helper.sendRequest("http://http://oss-api.aisaferjr.com/oss/api/entry?", "smjr.oss.test",
				content, httpParam);
		// String respon =
		// helper.sendRequest("http://192.168.14.26:9876/oss/api/entry?",
		// "smjr.oss.upload", content,httpParam);
		System.out.println(JSONUtil.toJson(respon));
	}

	public void downRemoteTest() throws IOException {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("md5Key", "FUND");
		// content.put("storageKey",
		// "20170410/1000000000000000000000000001/11d55b9380fd44779cab16240adb61a8.png");
		content.put("storageKey", "0fd6eeabaa2a42a69b6b3cecc880e097");
		content.put("modulekey", "FUND");
		SmjrApiHelper helper = new SmjrApiHelper("FUND", "tS5awNlr2O0odAWkgOad6g==", "FUND");
		// String respon =
		// helper.sendRequest("http://120.55.163.223:9876/oss/api/entry?",
		// "smjr.oss.download", content,new HttpParam());
		String respon = helper.sendRequest("http://139.224.8.8:9876/oss/api/entry?", "smjr.oss.download", content,
				new HttpParam());
		// String respon = helper.sendRequest("http://localhost:8083/api/entry?",
		// "smjr.oss.download", content,new HttpParam());
		// http://139.224.8.8:9876/oss/api/entry?appid=CFBS&method=smjr.oss.download&timestamp=1492764155403&version=1.0&content=jiOLwxQs1zWmZwM0LmzoVeXfa%2fcHUFrDvIGACvy%2f5BHCd%2f1waZrC4g4Bv3VzASpAAmZmsi2IdG9HYXYF7ubP03mtZm3D668HTU8FzUjUoow%3d
		// String respon =
		// helper.sendRequest("http://oss-api.aisaferjr.com/oss/api/entry?",
		// "smjr.oss.download", content,new HttpParam());
		Map<String, Object> map = JSONUtil.toMap(respon);
		byte[] str = Base64.decode(helper.aesDecrypt(String.valueOf(map.get("data"))));
		FileUtils.getFile().byte2File(str, "d:/file/", "10008.txt");
		// System.out.println(new String(str));
	}

	public void downRemoteByteTest() throws IOException {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("md5Key", "FUND");
		// content.put("storageKey",
		// "20170913/1010/fe1a081410ac4dc3bc27741e441df0b8.pdf");
		content.put("storageKey", "06955bb2169749c698e1fe4459b2f32b");
		content.put("modulekey", "FUND");
		SmjrApiHelper helper = new SmjrApiHelper("FUND", "tS5awNlr2O0odAWkgOad6g==", "FUND", "2.0");
		// String respon =
		// helper.sendRequest("http://120.55.163.223:9876/oss/api/entry?",
		// "smjr.oss.download", content,new HttpParam());
		String respon = helper.sendRequest("http://139.224.8.8:9876/oss/api/entry?", "smjr.oss.download", content,
				new HttpParam());
		// String respon = helper.sendRequest("http://localhost:8083/api/entry?",
		// "smjr.oss.download", content,new HttpParam());
		// String respon =
		// helper.sendRequest("http://oss-api.aisaferjr.com/oss/api/entry?",
		// "smjr.oss.download", content,new HttpParam());
		FileUtils.getFile().byte2File(Base64.decode(respon), "d:/file/", "100010.pdf");
		// System.out.println(new String(str));
	}

	@Test
	public void downloadTest() throws IOException {
		/*
		 * List<Attachment> attachmentList = attachmentDao.getList(new Attachment());
		 * for (Attachment atta : attachmentList) {
		 */
		DownReq req = new DownReq();
		// req.setAttachId("5b9f1652ddd74eeb9418fd30bc06c105");
		// req.setAttachid("20170823/20170823122304/4a91fe62b0144aa2a8ed5fb9ac57cd41.jpg");
		req.setModulekey("CFBS");
		req.setStorageKey("20170912/1010/d64438e268064a30b324dd29fe1e1e9b.pdf");
		// req.setModulekey("OSSTEST");
		// req.setStorageKey("20170412/1000000000000000000000000001/143f6f286b72430d8d64e2fafab7423b.pdf");
		long begin = System.currentTimeMillis();
		DownResp buf = service.download(req);
		/*
		 * FileUtils.getFile().byte2File(buf, "/data/attach/", "100002.jpg"); String
		 * base = Base64.encode(buf);
		 */
		System.out.println("time:" + (System.currentTimeMillis() - begin));

		// FileUtils.getFile().byte2File(buf, "/data/attach/", atta.getFileName());
		// }
	}

	public static void main(String[] args) throws ParseException, IOException {

		/*try {
			new OSSServiceTest().uploadRemoteTest2();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}*/

		// new OSSServiceTest().uploadRemoteTest();
		// new OSSServiceTest().downRemoteTest();
		// new OSSServiceTest().downRemoteByteTest();
		// new OSSServiceTest().uploadTest();
		// new OSSServiceTest().authurl();
		// new OSSServiceTest().downloadTest();
	}

}
