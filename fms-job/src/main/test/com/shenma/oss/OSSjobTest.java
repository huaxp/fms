package com.aisafer.oss;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.aisafer.aisaferJobApplication;
import com.aisafer.oss.service.impl.SynchronizationAttach;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = aisaferJobApplication.class)
public class OSSjobTest {

	@Autowired
	SynchronizationAttach sync;
	
	@Test
	public void syncTest() {
		sync.sync();
	}
}
