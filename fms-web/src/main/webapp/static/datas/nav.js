//供应链
var supply_chain = [{
	"title": "附件管理",
	"icon": "fa-cubes",
	"spread": true,
	"children": [{
		"title": "附件列表",
		"icon": "&#xe641;",
		"href": "/attach/list"
	}, {
		"title": "附件上传",
		"icon": "&#xe63c;",
		"href": "/attach/add"
	}]
}];

//渠道
var channel = [{
	"title": "基本元素",
	"icon": "fa-cubes",
	"spread": true,
	"children": [{
		"title": "按钮",
		"icon": "&#xe641;",
		"href": "button.html"
	}, {
		"title": "表单",
		"icon": "&#xe63c;",
		"href": "form.html"
	}, {
		"title": "表格",
		"icon": "&#xe63c;",
		"href": "table.html"
	}, {
		"title": "导航",
		"icon": "&#xe609;",
		"href": "nav.html"
	}, {
		"title": "辅助性元素",
		"icon": "&#xe60c;",
		"href": "auxiliar.html"
	}]
}, {
	"title": "组件",
	"icon": "fa-cogs",
	"spread": false,
	"children": [{
		"title": "BTable",
		"icon": "fa-table",
		"href": "btable.html"
	}, {
		"title": "Navbar组件",
		"icon": "fa-navicon",
		"href": "navbar.html"
	}, {
		"title": "Tab组件",
		"icon": "&#xe62a;",
		"href": "tab.html"
	}, {
		"title": "Laytpl+Laypage",
		"icon": "&#xe628;",
		"href": "paging.html"
	}]
}];

var merchant_account = [{
		"title": "账户查询",
		"icon": "fa-stop-circle",
		"href": "/static/merchant/account_inquiry.html",
		"spread": false
	},
	{
		"title": "交易查询",
		"icon": "fa-cubes",
		"href": "/static/merchant/account_inquiry.html",
		"spread": false
	},
	{
		"title": "冻结-解冻",
		"icon": "fa-cogs",
		"href": "https://www.baidu.com",
		"spread": false
	},
	{
		"title": "提现查询",
		"icon": "fa-address-book",
		"href": "https://www.baidu.com",
		"spread": false
	}
];
