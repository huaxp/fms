package com.aisafer.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component("ossConfig")
@ConfigurationProperties(prefix = "com.aisafer.oss",ignoreUnknownFields = true)
public class OssConfig {
	private Long urlDefalutExpire;
	private boolean enableConcurrentTest;
	private String maxFileSize;
	private String maxRequestSize;
	private String fileName;
	public Long getUrlDefalutExpire() {
		return urlDefalutExpire;
	}
	public void setUrlDefalutExpire(Long urlDefalutExpire) {
		this.urlDefalutExpire = urlDefalutExpire;
	}
	public boolean isEnableConcurrentTest() {
		return enableConcurrentTest;
	}
	public void setEnableConcurrentTest(boolean enableConcurrentTest) {
		this.enableConcurrentTest = enableConcurrentTest;
	}
	public String getMaxFileSize() {
		return maxFileSize;
	}
	public void setMaxFileSize(String maxFileSize) {
		this.maxFileSize = maxFileSize;
	}
	public String getMaxRequestSize() {
		return maxRequestSize;
	}
	public void setMaxRequestSize(String maxRequestSize) {
		this.maxRequestSize = maxRequestSize;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
