package com.aisafer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;

@MapperScan("com.aisafer.oss.mapper")
@SpringBootApplication
@ImportResource(value={"classpath:/config/spring-config.xml"})
public class WebApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		
		return application.sources(aisaferWebApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(aisaferWebApplication.class, args);
	}

}