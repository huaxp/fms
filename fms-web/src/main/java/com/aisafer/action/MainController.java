package com.aisafer.action;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class MainController {
	
	@RequestMapping("/main")
	public String main(Map<String, Object> model) {
		return "main"; 
	}
	
	@RequestMapping("")
	public String main3(Map<String, Object> model) {
		return "main"; 
	}
	
	@RequestMapping("/")
	public String main1(Map<String, Object> model) {
		return "main"; 
	}
	
	
	@RequestMapping("/index")
	public String main2(Map<String, Object> model) {
		return "main"; 
	}
}
