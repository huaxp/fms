package com.aisafer.action;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/content")
public class ContentController {
	
	@RequestMapping("")
	public String content(Map<String, Object> model) {
		return "content"; 
	}
}
