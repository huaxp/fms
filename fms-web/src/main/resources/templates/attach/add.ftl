<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>新增附件</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
		<link rel="stylesheet" href="/static/plugins/font-awesome/css/font-awesome.min.css">
	</head>

	<body>
		<div style="margin: 15px;">
			<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
				<legend>响应式的表单集合</legend>
			</fieldset>

				 <form action="/attach/upload/"  method="post" enctype="multipart/form-data">  
                    <div class="layui-fluid">
                        <div class="layui-row">
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">来源编号:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="sourceCode" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">来源系统:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="sourceSystem" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                             <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">业务类型 :</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="categoryType" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-row">
                        	 <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">模块key :</label>
                                    <div class="layui-input-block">
                                         <select name="moduleKey" lay-filter="aihao">
											<option value=""></option>
											<#list moduleList as module>
												<option value="${module.moduleKey}">${module.moduleKey}</option>
											</#list>
										</select>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">附件类型:</label>
                                    <div class="layui-input-block">
                                    	<input type="text" name="fileType" placeholder="请输入" autocomplete="off" class="layui-input">   
                                    </div>
                                </div>
                            </div>
                           	 <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">附件:</label>
                                    <div class="layui-input-block">
                                    	<input type="file" name="file"  class="layui-btn">   
                                    </div>
                                </div>
                            </div>
                        </div>
 
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button type="submit" class="layui-btn">立即提交</button>
						<button type="reset" class="layui-btn >重置</button>
					</div>
				</div>
			</form>
			  
		</div>
		<script>
			
		</script>
	</body>

</html>