<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>btable</title>
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/plugins/font-awesome/css/font-awesome.min.css" />
</head>

<body style="padding: 0 5px;">
    <div class="layui-tab layui-tab-card">
        <ul class="layui-tab-title">
            <li class="layui-this">新增附件</li>
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <form class="layui-form" action="">
                    <div class="layui-fluid">
                        <div class="layui-row">
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">ossKey:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="ossKey" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">来源编号:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="sourceCode" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">来源系统:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="sourceSystem" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-row">
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">业务类型 :</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="categoryType" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">开始时间:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="startTime" id="startTime" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">结束时间:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="endTime" id="endTime" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                    <blockquote class="layui-elem-quote">
                        <button type="button" class="layui-btn layui-btn-small" id="add"><i class="fa fa-plus" aria-hidden="true"></i> 新增附件</button>
                        <button type="button" lay-filter="search" class="layui-btn layui-btn-small" lay-submit style="float:right;"><i class="fa fa-search" aria-hidden="true"></i> 查询</button>
                    </blockquote>
                </form>
                <div id="content">
                    <table id="demo" lay-filter="test" style="width: 100%;"></table>
                    <script type="text/html" id="barDemo">
                        <a class="layui-btn layui-btn-mini" lay-event="download">下载</a>
                    </script>
                </div>
            </div>
            <div class="layui-tab-item">2</div>
        </div>
    </div>
    <script type="text/javascript" src="/static/plugins/layui/layui.js"></script>
    <script>
    layui.use(['form', 'element', 'jquery', 'laydate', 'layer', 'table'], function() {
        var $ = layui.jquery,
            element = layui.element,
            layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
            layer = layui.layer, //获取当前窗口的layer对象;
            form = layui.form,
            table = layui.table,
            laydate = layui.laydate;
        //初始化日期插件
        laydate.render({
            elem: '#startTime', //指定元素
            //range: '~',
            calendar: true
        });
        laydate.render({
            elem: '#endTime', //指定元素
            //range: '~',
            calendar: true
        });
        //表格--执行渲染
        var options = {
            id: 'idTest',
            elem: '#demo', //指定原始表格元素选择器（推荐id选择器）
            url: '/attach/getList',
            // where: {token: 'sasasas', id: 123} //如果无需传递额外参数，可不加该参数
            // method: 'post' //如果无需自定义HTTP类型，可不加该参数
            // request: {} //如果无需自定义请求参数，可不加该参数
            // response: {} //如果无需自定义数据响应名称，可不加该参数
            page: true,
            cols: [
                [
                    { title: '操作', align: 'center', width: 80, toolbar: '#barDemo' },
                    { field: 'id', title: 'ossKey', align: 'center', width: 250 },
                    { field: 'sourceCode', title: '来源编号', width: 200, edit: 'text' },
                    { field: 'sourceSystem', title: '来源系统', width: 100 },
                    { field: 'categoryType', title: '业务类型', width: 100 },
                    { field: 'fileName', title: '附件名字', width: 200 },
                    { field: 'fileSize', title: '附件大小', width: 150 },
                    { field: 'createTime', title: '创建日期', width: 200 }
                ]
            ], 
           
            //数据渲染完的回调
            done: function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                // console.log(res);
                
                //得到当前页码
                // console.log(curr); 
                
                //得到数据总量
                // console.log(count);
            }
        };
        table.render(options);

        //监听工具条
        table.on('tool(test)', function(obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            if (layEvent === 'download') { //查看
               window.location.href="/attach/download?attachid="+data.id; 
            }
        });

        //自定义验证规则
        form.verify({
            phone: function(value){
                if($.trim(value) === ''){

                } else{
                    var regex = /^1\d{10}$/;
                    if(!value.match(regex)) {
                        return '请输入正确的手机号';
                    }
                }
            }
        });
        //监听搜索表单的提交事件
        form.on('submit(search)', function(data) {
            console.log(data);
            //表格重载
            options.where = data.field;
            table.reload('idTest', options);
            return false;
        });

        //新增授信申请
        $('#add').on('click', function () {
            parent.tab.tabAdd({
                href: '/attach/add', //地址
                icon: 'fa fa-plus',
                title: '新增授信申请'
            });
        });
    });
    </script>
</body>

</html>
