<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>btable</title>
    <meta name="renderer" content="webkit" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/plugins/font-awesome/css/font-awesome.min.css" />
</head>

<body style="padding: 0 5px;">
    <div class="layui-tab layui-tab-card">
        <ul class="layui-tab-title">
            <li class="layui-this">授信申请列表</li>
            <li>我的授信申请</li>
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <form class="layui-form" action="">
                    <div class="layui-fluid">
                        <div class="layui-row">
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">产品类型:</label>
                                    <div class="layui-input-block">
                                        <select name="ptype">
                                            <option value=""></option>
                                            <option value="xyd">信用贷</option>
                                            <option value="xsd">新生贷</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">申请单号:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="requestNo" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">经销商名称:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="dealerName" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-row">
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">准入方式:</label>
                                    <div class="layui-input-block">
                                        <select name="accessMode">
                                            <option value=""></option>
                                            <option value="0">线上</option>
                                            <option value="1">线下</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">手机号:</label>
                                    <div class="layui-input-block">
                                        <input type="tel" name="telephone" lay-verify="phone" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">身份证号:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="identity" lay-verify="identity" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-row">
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">提交时间:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="subTime" id="subTime" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">合伙人:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="copartner" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md4">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">操作人:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="operater" placeholder="请输入" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-row">
                            <div class="layui-col-md12">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">地区:</label>
                                    <div class="layui-input-inline">
                                        <select name="province"></select>
                                    </div>
                                    <div class="layui-form-mid">-</div>
                                    <div class="layui-input-inline">
                                        <select name="city"></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <blockquote class="layui-elem-quote">
                        <button type="button" class="layui-btn layui-btn-small" id="add"><i class="fa fa-plus" aria-hidden="true"></i> 新增授信申请</button>
                        <button type="button" lay-filter="search" class="layui-btn layui-btn-small" lay-submit style="float:right;"><i class="fa fa-search" aria-hidden="true"></i> 查询</button>
                    </blockquote>
                </form>
                <div id="content">
                    <table id="demo" lay-filter="test" style="width: 100%;"></table>
                    <script type="text/html" id="barDemo">
                        <a class="layui-btn layui-btn-mini" lay-event="detail">查看</a>
                    </script>
                </div>
            </div>
            <div class="layui-tab-item">2</div>
        </div>
    </div>
    <script type="text/javascript" src="/static/plugins/layui/layui.js"></script>
    <script>
    layui.use(['form', 'element', 'jquery', 'laydate', 'layer', 'table'], function() {
        var $ = layui.jquery,
            element = layui.element,
            layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
            layer = layui.layer, //获取当前窗口的layer对象;
            form = layui.form,
            table = layui.table,
            laydate = layui.laydate;
        //初始化日期插件
        laydate.render({
            elem: '#subTime', //指定元素
            range: '~',
            calendar: true
        });
        //表格--执行渲染
        var options = {
            id: 'idTest',
            elem: '#demo', //指定原始表格元素选择器（推荐id选择器）
            url: '../../datas/demo.json',
            // where: {token: 'sasasas', id: 123} //如果无需传递额外参数，可不加该参数
            // method: 'post' //如果无需自定义HTTP类型，可不加该参数
            // request: {} //如果无需自定义请求参数，可不加该参数
            // response: {} //如果无需自定义数据响应名称，可不加该参数
            page: true,
            cols: [
                [
                    { title: '操作', align: 'center', width: 80, toolbar: '#barDemo' },
                    { field: 'index', title: '序号', align: 'center', width: 80 },
                    { field: 'id', title: 'ID', width: 100 },
                    { field: 'username', title: '用户名', width: 200, edit: 'text' },
                    { field: 'sex', title: '性别', width: 80 },
                    { field: 'city', title: '签名', width: 200 },
                    { field: 'sign', title: '城市', width: 150 },
                    { field: 'experience', title: '测试', width: 150 },
                    { field: 'logins', title: '登陆', width: 200 },
                    { field: 'wealth', title: '天气', width: 200 },
                    { field: 'score', title: '分数', width: 200 },
                    { field: 'classify', title: '分类', width: 200, fixed: 'right' }
                ]
            ], //设置表头
            responseDataHandler: function(res){
                res.filter(function(item, index, items){
                    item.index = index+1;
                    let {sex} = item;
                    item.sex = sex == '女'? '0' : '1';
                });
                return res;
            },
            //数据渲染完的回调
            done: function(res, curr, count){
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                // console.log(res);
                
                //得到当前页码
                // console.log(curr); 
                
                //得到数据总量
                // console.log(count);
            }
        };
        table.render(options);

        //监听工具条
        table.on('tool(test)', function(obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值
            var tr = obj.tr; //获得当前行 tr 的DOM对象

            if (layEvent === 'detail') { //查看
                //do somehing
                //console.log(obj);
            }
        });

        //自定义验证规则
        form.verify({
            phone: function(value){
                if($.trim(value) === ''){

                } else{
                    var regex = /^1\d{10}$/;
                    if(!value.match(regex)) {
                        return '请输入正确的手机号';
                    }
                }
            }
        });
        //监听搜索表单的提交事件
        form.on('submit(search)', function(data) {
            console.log(data);
            //表格重载
            options.where = data.field;
            table.reload('idTest', options);
            return false;
        });

        //新增授信申请
        $('#add').on('click', function () {
            parent.tab.tabAdd({
                href: 'supplychain/credit/new_credit.html', //地址
                icon: 'fa fa-plus',
                title: '新增授信申请'
            });
        });
    });
    </script>
</body>

</html>
