-- MySQL dump 10.13  Distrib 5.6.36, for Linux (x86_64)
--
-- Host: 172.16.10.23    Database: oss_20171122
-- ------------------------------------------------------
-- Server version	5.6.35-log



DROP TABLE IF EXISTS `oss_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oss_attachment` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `file_name` varchar(100) DEFAULT NULL COMMENT '文件名称',
  `file_type` varchar(100) DEFAULT NULL COMMENT '类型',
  `internal_storage_no` varchar(500) DEFAULT NULL COMMENT '地址',
  `ext1` varchar(300) DEFAULT NULL COMMENT '扩展1',
  `ext2` varchar(300) DEFAULT NULL COMMENT '扩展2',
  `ext3` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `module_id` varchar(32) DEFAULT NULL COMMENT '模块外键',
  `category_type` varchar(50) DEFAULT NULL COMMENT '分类类型',
  `source_code` varchar(50) DEFAULT NULL COMMENT '来源编号',
  `external_storage_no` varchar(500) DEFAULT NULL COMMENT '存储放在云端健',
  `file_size` int(11) DEFAULT NULL COMMENT '文件大小',
  `batch_num` varchar(50) DEFAULT NULL COMMENT '批次号',
  `lasttime` bigint(11) DEFAULT NULL,
  `attach_hash` varchar(500) DEFAULT NULL,
  `is_encrypt` char(1) DEFAULT NULL COMMENT 'Y:是，N：否',
  `encrypt_model` varchar(10) DEFAULT NULL COMMENT '加密方式',
  `etag` varchar(100) DEFAULT NULL,
  `secret_key` varchar(200) DEFAULT NULL,
  `is_compress` char(1) DEFAULT NULL,
  `compress_model` varchar(10) DEFAULT NULL,
  `cloud_path` varchar(500) DEFAULT NULL,
  `local_path` varchar(500) DEFAULT NULL,
  `source_system` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `create_time_index` (`create_time`) USING BTREE,
  KEY `code_sys_type_index` (`source_code`,`source_system`,`category_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='附件表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oss_attachment_0`
--



--
-- Table structure for table `oss_attachment_e`
--

DROP TABLE IF EXISTS `oss_attachment_e`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oss_attachment_e` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `attachid` varchar(32) DEFAULT NULL COMMENT 'attachid',
  `storage_type` varchar(20) DEFAULT NULL COMMENT 'ALYUN,HDFS,LOCAL',
  `storage_key` varchar(120) DEFAULT NULL COMMENT '存储健',
  `tag` varchar(100) DEFAULT NULL COMMENT '标识',
  `status` char(1) DEFAULT NULL COMMENT 'S:成功，F:失败，I：初始化',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `lasttime` bigint(20) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `create_time_index` (`create_time`),
  KEY `idx_attachid` (`attachid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='附件扩展表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oss_attachment_g`
--

DROP TABLE IF EXISTS `oss_attachment_g`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oss_attachment_g` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `attachid` varchar(32) DEFAULT NULL COMMENT 'attachid',
  `groupid` varchar(32) DEFAULT NULL COMMENT '附件组ID',
  `file_block` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `lasttime` bigint(20) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oss_module`
--

DROP TABLE IF EXISTS `oss_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oss_module` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `module_key` varchar(100) DEFAULT NULL COMMENT '模块键',
  `is_enable` char(1) DEFAULT NULL COMMENT '启用：（E:启用;D:关闭）',
  `module_desc` varchar(200) DEFAULT NULL COMMENT '模块描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `secret_key` varchar(200) DEFAULT NULL,
  `url_defalut_expire` bigint(20) DEFAULT NULL,
  `rsa_public_key` varchar(1000) DEFAULT NULL,
  `rsa_private_key` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模块';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oss_sts_config`
--

DROP TABLE IF EXISTS `oss_sts_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oss_sts_config` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `access_key_id` varchar(50) DEFAULT NULL COMMENT '接收键',
  `access_key_secret` varchar(100) DEFAULT NULL COMMENT '接收值',
  `access_lan_url` varchar(100) DEFAULT NULL COMMENT '接收URL',
  `security_token` varchar(32) DEFAULT NULL COMMENT '认证令牌',
  `auth_desc` varchar(100) DEFAULT NULL COMMENT '认证描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `bucket_name` varchar(50) DEFAULT NULL COMMENT '名称',
  `attach_expiration` int(11) DEFAULT NULL COMMENT '过期时间（单位：秒）（-1）表示不设置过期时间',
  `method_type` varchar(10) DEFAULT NULL COMMENT '请求类型',
  `name` varchar(50) DEFAULT NULL,
  `access_wan_url` varchar(100) DEFAULT NULL,
  `module_id` varchar(32) DEFAULT NULL COMMENT '请求过期时间',
  `request_expiration` int(11) DEFAULT NULL COMMENT '请求过期时间（秒为单位，-1为不过期）',
  `cloud_storage_mode` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='认证参数配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oss_sts_param_config`
--

DROP TABLE IF EXISTS `oss_sts_param_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oss_sts_param_config` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `config_key` varchar(50) DEFAULT NULL COMMENT '键',
  `config_value` varchar(100) DEFAULT NULL COMMENT '值',
  `module_id` varchar(32) DEFAULT NULL COMMENT '模块外键',
  `config_desc` varchar(100) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='参数配置表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-07  2:14:19
