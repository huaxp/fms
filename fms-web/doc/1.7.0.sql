drop table if exists oss_attachment_e;
create table oss_attachment_e
(
   id                   varchar(32) not null comment 'id',
   attachid             varchar(32) comment 'attachid',
   storage_type         varchar(20) comment 'ALYUN,HDFS,LOCAL',
   storage_key	        varchar(120) comment '存储健',
   tag                  varchar(100) comment '标识',
   status               char(1) comment 'S:成功，F:失败，I：初始化',
   create_time          datetime comment '创建时间',
   lasttime        	bigint,
   update_time          datetime comment '修改时间'
);

alter table oss_attachment_e comment '附件扩展表';
alter table oss_attachment_e   add primary key (id);


alter table oss_attachment_e add index attachid_index(attachid);

alter table oss_attachment_e add index attachid_index(storage_key);

alter table oss_attachment_e add index storage_type_index(storage_type);


alter table oss_attachment add index create_time_index(create_time);

alter table oss_attachment   add column source_system varchar(100);

alter table oss_attachment add index source_code_index(source_code);
