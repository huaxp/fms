alter table oss_module   add column rsa_public_key varchar(1000);
alter table oss_module   add column rsa_private_key varchar(2000);

update oss_module set rsa_public_key='MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCtRYJ5tgs9HwDsDVRxtLDvjvZtjkSOpa+A/OPFBqvlUmMFt2U32BkJGCaiEj5KfkqBkR0vBJEdgx89Qfpr4d/ps+rtf+1hrWWJln3e1ypsHxsVdVBHFC4PB3vU+FDUFbScbse/xMxLFBgTOr9lghWK/lDDAnGQAceEA0jv2TudawIDAQAB';
update oss_module set rsa_private_key='MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAK1Fgnm2Cz0fAOwNVHG0sO+O9m2ORI6lr4D848UGq+VSYwW3ZTfYGQkYJqISPkp+SoGRHS8EkR2DHz1B+mvh3+mz6u1/7WGtZYmWfd7XKmwfGxV1UEcULg8He9T4UNQVtJxux7/EzEsUGBM6v2WCFYr+UMMCcZABx4QDSO/ZO51rAgMBAAECgYEArTE0aLnY5AAwaMIuLPWigeFRWlKBHmBoW7LrbE3t1euSxOz6hGZBKAD25Lr9fnJbwMSu+gdDkc81k4J2cA03RHQsHkVNDyB6k7tXJRS9cnlpkuNF6YFjp9pbldp/6Qj9JnT5KLMtcX7fxXuCRH0mwrSlTrIu5/kYKy3mzPktIBECQQD5LRvoIf9qABYIz9L2GvYSVGmAiKPOjl6uV74wF+m4ny09LJspD0nyxbcMbM5iIbagNM7N0/9SGrkafP4j/W/ZAkEAsgRCIR+cYHEJp8Qf9XM+I5U1DXaiUTbXOogu6N7ZoEncr+LVDmPZW3+u9Wtk79khfHBgbWJk07S7iPwcShbw4wJATYU4KmbxVGQEdudZJZHLvnYegIuCEs/+KtVHaST3ZKW46FBJUWPdSLc0NJxhNOxyKbNkJ2ruAvgnYFNEqaO+OQJAUeEjiZjAlJtplYDgMkWPj05R2J94iWJwHLwG15WpA9D1cPEkjjne//b6wmdjRe7ZOCN4NtfLCJVYrOlJGcrzgQJBALekfiVFWafhc0D2mpNb0HYhwqlDv57TP4p8GBW8MaYFwzn6RCoqKXhOOgErzWex5Vjw8+Jhx/cS+m+z/V1oe/Q=';

alter table oss_attachment   modify column ext3 varchar(300);
alter table oss_sts_config   add column cloud_storage_mode varchar(20);
update oss_sts_config set cloud_storage_mode='ALIYUN';


drop table if exists oss_attachment_g;
create table oss_attachment_g
(
   id                   varchar(32) not null comment 'id',
   attachid             varchar(32) comment 'attachid',
   groupid		        varchar(32) comment '附件组ID',
   file_block        	bigint,
   create_time          datetime comment '创建时间',
   lasttime        		bigint,
   update_time          datetime comment '修改时间'
);
alter table oss_attachment_g comment '附件組表';
alter table oss_attachment_g   add primary key (id);
#清空redis缓存
