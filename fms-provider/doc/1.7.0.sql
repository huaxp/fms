alter table oss_attachment   add column source_system varchar(100);


alter table oss_attachment add index create_time_index(create_time);

alter table oss_attachment add index code_sys_type_index(source_code,source_system,categoryType);

alter table oss_attachment add index source_code_index(source_code);


alter table oss_attachment add index code_sys_type_index(source_code,source_system,category_type);

