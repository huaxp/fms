package com.aisafer.oss.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.aisafer.ProviderApplication;
import com.aisafer.fms.domain.Attachment;
import com.aisafer.fms.dto.AttachmentHandleReq;
import com.aisafer.fms.service.impl.AttachmentHandleSercie;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProviderApplication.class)
public class AttachmentHandleTest {
	
	@Autowired
	AttachmentHandleSercie service;
	
	@Test
	public void listTest() {
		AttachmentHandleReq req = new AttachmentHandleReq();
		req.setOssKey("0000ac8568134d6b960dfdd73c9207d6");
		List<Attachment> list = service.getAttachmentList(req);
		Assert.assertNotNull(list);
		
	}
	
	@Test
	public void listTest1() {
		AttachmentHandleReq req = new AttachmentHandleReq();
		req.setStartTime("2017-09-01");
		req.setEndTime("2017-10-26");
		List<Attachment> list = service.getAttachmentList(req);
		Assert.assertNotNull(list);
		
	}
}
