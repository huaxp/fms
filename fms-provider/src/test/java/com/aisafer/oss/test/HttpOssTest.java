/*package com.aisafer.oss.test;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.aisafer.oss.utils.Base64;
import com.aisafer.oss.utils.FileTypeJudge;
import com.aisafer.oss.utils.HttpUtil;
import com.aisafer.oss.utils.JSONUtil;


public class HttpOssTest {
	
	//String path = "d:/doc/互联网研发管理 Vv1/(2).pdf";
	//String path = "d:/file/100016.jpeg";
	//String path = "d:/doc/放款结果导入新.xlsx";
	String path = "d:/1.png";
	//String path = "D:/doc/1.rar";
	String key = "";
	String storagePath = "d:/file/";
	@Test
	public void down() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("attachid", "fbd28e2616b64b2687e2fee25b721bf7");
		try {
			final String result = HttpUtil.post(getMap("pro")+"/download/v1/?", content);
			FileUtils.getFile().byte2File(Base64.decode(result), "d:/file/", "100016."+FileTypeJudge.getType(Base64.decode(result)));
			Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void downbyte() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("attachid", "e21ef2339f8f48e5b055a45ddaf0dc73");
		try {
			final byte[] result = HttpUtil.postByte(getMap("uat")+"/downloadbyte/v1/?", content);
			FileUtils.getFile().byte2File(result, "d:/file/", "100017."+FileTypeJudge.getType(result));
			Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void downfile() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("attachid", "20171220/c8a26cf2ef674089ba17fcf0db1f22a9/8188cdfd184342d7a6d9916ed18568fd.pdf");
		content.put("categoryType", "CFBS");
		try {
			final byte[] result = HttpUtil.postByte(getMap("dev")+"/downloadfile/v1/?", content);
			FileUtils.getFile().byte2File(result, "d:/file/", "100017."+FileTypeJudge.getType(result));
			Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void view() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("attachid", "20170524/8f73e01625294372b6d012ba1ae436a5/e55d67be-3572-4dce-ba17-de941b9c337e.jpg");
		content.put("width", "300");
		content.put("height", "300");
		try {
			final byte[] result = HttpUtil.postByte(getMap("pro")+"/view/v1//?", content);
			FileUtils.getFile().byte2File(result, "d:/file/", "100019."+FileTypeJudge.getType(result));
			Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Test
	public void down2() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("attachid", "20170524/8f73e01625294372b6d012ba1ae436a5/91f2747f-68ef-417a-a4d8-a6dde0a18a61.jpg");
		content.put("modulekey", "CFBS");
		//content.put("sourceSystem", "C");
		try {
			final String result = HttpUtil.post(getMap("pro")+"/download/v1/?", content);
			FileUtils.getFile().byte2File(Base64.decode(result), "d:/file/", "100016."+FileTypeJudge.getType(Base64.decode(result)));
			Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void down3() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("attachid", "7ac89e495a3e4011a60b828c478e0fcb");
		content.put("modulekey", "CFBS");
		try {
			final String result = HttpUtil.post(getMap("sit")+"/download/v1/?", content);
			FileUtils.getFile().byte2File(Base64.decode(result), "d:/file/", "100015."+FileTypeJudge.getType(Base64.decode(result)));//
			Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void down4() {
		try {
			String fileName = "";
			FileUtils.getFile().byte2File(Base64.decode(fileName), "d:/file/", "100015."+FileTypeJudge.getType(Base64.decode(fileName)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void authUrl() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("attachid", "df2345a9ac4244ac826450e4056ebe0c");
		//content.put("attachid", "8a7cfbb50ccd4acaa8133625ff0b5764");
		content.put("urlExpire", "1200");
		content.put("urlType", "LOCAL");
		//content.put("height", "500");
		//content.put("width", "500");
		
		try {
			final String result = HttpUtil.get(getMap("pro")+"/authurl/v1/?", content);
			System.out.println("auth:"+JSONUtil.toJson(result));
			Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("");
	}



	@Test
	public void uploadpng() {
		
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("sourceCode", "1000009");
			param.put("fileType", ".png");
			param.put("urlExpire", "3600");// url访问过期时间
			param.put("fileType", "png");
			param.put("fileName", "1001.png");
			param.put("categoryType", "pic");
			param.put("moduleKey", "SCF");
			param.put("sourceSystem", "C");
			Map<String,byte[]> content = new HashMap<String,byte[]>();
			content.put("file", FileUtils.getFile().getFilebyte(path));
			//final String result = HttpClientUtils.postForm(getMap("sit")+"/upload/v1/?", param, new HttpParam().getHeaders(), "UTF-8", 10000, 10000,content);
			//System.out.println(result);
			//Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void uploadpdf() {
		
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("sourceCode", "1000009");
			param.put("fileType", ".png");
			param.put("urlExpire", "3600");// url访问过期时间
			param.put("fileType", "png");
			param.put("fileName", "1001.pnhg");
			param.put("categoryType", "pic");
			param.put("moduleKey", "RISK");
			param.put("sourceSystem", "C");
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			Map<String,byte[]> content = new HashMap<String,byte[]>();
			//content.put("file", FileUtils.getFile().getFilebyte(path));
			//final String result = HttpClientUtils.postForm(getMap("dev")+"/upload/v1/?", param, new HttpParam().getHeaders(), "UTF-8", 10000, 10000,content);
			//System.out.println(result);
			//Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getMap(String key) {
		Map<String, String> map = new HashMap<>();
		map.put("dev", "http://127.0.0.1:8083/api/entry/");
		map.put("sit04", "http://172.16.10.70:8094/oss/api/entry/");
		map.put("sit05", "http://172.16.10.77:8094/oss/api/entry/");
		map.put("sit", "http://172.16.10.64:8094/oss/api/entry/");
		map.put("sit01", "http://172.16.10.94:8094/oss/api/entry/");
		map.put("uat", "http://172.16.10.56:8083/oss/api/entry/");
		map.put("pre", "http://172.16.10.86:8094/oss/api/entry/");
		map.put("perf", "http://172.16.10.5:8160/oss/api/entry/");
		map.put("devs", "http://192.168.10.240:4080/oss/api/entry/");
		map.put("pro", "http://oss-api.shenmajr.com/oss/api/entry/");
		return map.get(key);
	}
}*/