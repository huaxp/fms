package com.aisafer.oss.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.aisafer.ProviderApplication;
import com.aisafer.base.pojo.Response;
import com.aisafer.fms.client.dto.AttachmentAdditionalContent;
import com.aisafer.fms.client.dto.AttachmentAdditionalReq;
import com.aisafer.fms.client.dto.AttachmentContent;
import com.aisafer.fms.client.dto.AttachmentReq;
import com.aisafer.fms.client.dto.AttachmentResp;
import com.aisafer.fms.client.dto.AuthReq;
import com.aisafer.fms.client.dto.DownGroupReq;
import com.aisafer.fms.client.dto.DownReq;
import com.aisafer.fms.client.dto.GroupReq;
import com.aisafer.fms.client.dto.Order;
import com.aisafer.fms.client.enums.StorageTypeEnums;
import com.aisafer.fms.client.enums.ViewMode;
import com.aisafer.fms.client.service.AttachmentBlockHandler;
import com.aisafer.fms.client.service.AttachmentGroupService;
import com.aisafer.fms.client.service.AttachmentService;
import com.polaris.base.utils.string.JsonUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProviderApplication.class)
public class AttachmentServiceTest {

	@Autowired
	AttachmentService service;
	
	@Autowired
	AttachmentGroupService attachmentGroupService;

	//String path = "/home/stevin/2324234.pdf";
	String path = "d:/1.png";
	//String path = "d:/1.mp4";
	//String path = "d:/txt.txt";
	String key = "";
	String storagePath = "d:/file/";

	//@Autowired
	AttachmentBlockHandler attachmentBlockHandler;
	
	@Before
	public void init() {
	}
	
	
	@Test
	public void uploadFileTest() {
		AttachmentReq attReq = new AttachmentReq();
		attReq.setModuleKey("TEST");
		//attReq.setSourceCode("1000000000000000000000000001");
		AttachmentContent attachmentContent = null;
		attachmentContent = new AttachmentContent();
		attachmentContent.setContent(FileUtils.getFile().getFilebyte(path));
		attachmentContent.setFileSize(Integer.valueOf(attachmentContent.getContent().length).longValue());
		attachmentContent.setFileType("png");
		attachmentContent.setFileName("tem.png");
		attachmentContent.setSourceCode("100007");
		attReq.setAttachmentContent(attachmentContent);
		Response<AttachmentResp> resp = service.uploadFile(attReq);
		System.out.println(JsonUtil.bean2Json(resp));
	}
	
	@Test
	public void uploadFileTest1() {
		AttachmentReq attReq = new AttachmentReq();
		attReq.setModuleKey("TEST");
		//attReq.setSourceCode("1000000000000000000000000001");
		AttachmentContent attachmentContent = null;
		attachmentContent = new AttachmentContent();
		attachmentContent.setContent(FileUtils.getFile().getFilebyte(path));
		attachmentContent.setFileSize(Integer.valueOf(attachmentContent.getContent().length).longValue());
		attachmentContent.setFileType("mp4");
		attachmentContent.setFileName("1.mp4");
		attachmentContent.setSourceCode("100007");
		attReq.setAttachmentContent(attachmentContent);
		Response<AttachmentResp> resp = service.uploadFile(attReq);
		System.out.println(JsonUtil.bean2Json(resp));
	}
	
	@Test
	public void uploadFileTestFtp() {
		AttachmentReq attReq = new AttachmentReq();
		attReq.setModuleKey("TEST");
		AttachmentContent attachmentContent = null;
		attachmentContent = new AttachmentContent();
		attachmentContent.setSourceCode("100007");
		attachmentContent.setStorageKey("20180828/100007/1034384430086230016.png");
		attachmentContent.setStorageType(StorageTypeEnums.ALIYUN);
		attReq.setAttachmentContent(attachmentContent);
		Response<AttachmentResp> resp = service.uploadFile(attReq);
		System.out.println(JsonUtil.bean2Json(resp));
	}
	
	@Test
	public void uploadMergeFileTest() {
		String batchid = String.valueOf(System.currentTimeMillis());
		Order order = null;
		byte[] content = FileUtils.getFile().getFilebyte("d:/1.png");
		System.out.println(content.length);
		AttachmentAdditionalReq attReq = new AttachmentAdditionalReq();
		attReq.setModuleKey("TEST");
		AttachmentAdditionalContent attachmentContent = null;
		attachmentContent = new AttachmentAdditionalContent();
		order = new Order();
		order.setOrder(1);
		attachmentContent.setOrder(order);
		attachmentContent.setBatchid(batchid);
		attachmentContent.setContent(content(content, 0, 20000));
		attachmentContent.setFileSize(Integer.valueOf(attachmentContent.getContent().length).longValue());
		attachmentContent.setFileType("png");
		attachmentContent.setFileName("tem.png");
		attReq.setAttachmentContent(attachmentContent);
		attachmentContent.setSourceCode("1001");
	    service.uploadAdditionalFile(attReq);
		
		
		attReq = new AttachmentAdditionalReq();
		attReq.setModuleKey("TEST");
		attachmentContent = new AttachmentAdditionalContent();
		order = new Order();
		order.setOrder(2);
		attachmentContent.setOrder(order);
		attachmentContent.setBatchid(batchid);
		attachmentContent.setContent(content(content, 20000, 40000));
		attachmentContent.setFileSize(Integer.valueOf(attachmentContent.getContent().length).longValue());
		attachmentContent.setFileType("png");
		attachmentContent.setFileName("tem.png");
		attachmentContent.setSourceCode("1001");
		attReq.setAttachmentContent(attachmentContent);
		service.uploadAdditionalFile(attReq);
		
		attReq = new AttachmentAdditionalReq();
		attReq.setModuleKey("TEST");
		attachmentContent = new AttachmentAdditionalContent();
		order = new Order();
		order.setOrder(3);
		attachmentContent.setOrder(order);
		attachmentContent.setBatchid(batchid);
		attachmentContent.setContent(content(content, 40000, 93233));
		attachmentContent.setFileSize(Integer.valueOf(attachmentContent.getContent().length).longValue());
		attachmentContent.setFileType("png");
		attachmentContent.setFileName("tem.png");
		attachmentContent.setSourceCode("1001");
		attReq.setAttachmentContent(attachmentContent);
		service.uploadAdditionalFile(attReq);
		Response<AttachmentResp> resp = service.uploadMergeFile(attReq);
		System.out.println(JsonUtil.bean2Json(resp));
	}
	
	private byte[] content(byte[] content,int start,int end) {
		byte[] value = new byte[end-start];
		int c=0;
		for(int i=start;i<end;i++) {
			value[c] = content[i];
			c++;
		}
		return value;
	}
	
	@Test
	public void uploadFileGroupTest() {
		AttachmentReq attReq = new AttachmentReq();
		attReq.setModuleKey("CFBS");
		//attReq.setSourceCode("1000000000000000000000000001");
		AttachmentContent attachmentContent = null;

		attachmentContent = new AttachmentContent();
		//attachmentContent.setContent(FileUtils.getFile().getFilebyte(path));
		//attachmentContent.setFileSize(Integer.valueOf(attachmentContent.getContent().length).longValue());
		attachmentContent.setFileType("png");
		attachmentContent.setFileName("tem.png");
		attachmentContent.setSourceCode("100007");
		attReq.setAttachmentContent(attachmentContent);
		GroupReq groupReq = new GroupReq();
		groupReq.setBlockSize(204800);
		groupReq.setShardsNum(10);
		//groupReq.setBlockSize(2097152);
		Response<AttachmentResp> resp = attachmentBlockHandler.uploadFile(attReq, groupReq);
		System.out.println(JsonUtil.bean2Json(resp));
	}
	
	@Test
	public void downloadGroupFileTest() throws IOException {
		DownGroupReq req = new DownGroupReq();
		req.setAttachid(1034380917201440768l);
		long begin = System.currentTimeMillis();
		byte[] buf = attachmentBlockHandler.downloadFileByte(req);
		System.out.println("length:" + buf.length);
		/*FileUtils.getFile().byte2File(buf, storagePath, "100002."+FileTypeJudge.getType(buf));
		System.out.println(FileTypeJudge.getType(buf));*/
		System.out.println("time:" + (System.currentTimeMillis() - begin));
	}

	@Test
	public void downloadFileTest() throws IOException {
		DownReq req = new DownReq();
		req.setAttachid(1016870893319360512l);
		//req.setStorageKey("af94e5f73a9441d9ba269502d7c3ce0a");
		req.setModulekey("TEST");
		long begin = System.currentTimeMillis();
		byte[] buf = service.downloadFileByte(req);
		System.out.println("length:" + buf.length);
		FileUtils.getFile().byte2File(buf, storagePath, "100002.txt");
		/*System.out.println(FileTypeJudge.getType(buf));*/
		System.out.println("time:" + (System.currentTimeMillis() - begin));
	}
	
	@Test
	public void authUrl() throws IOException {
		AuthReq req = new AuthReq();
		req.setAttachid(10000l);
		req.setModulekey("CFBS");
		long begin = System.currentTimeMillis();
		Response<String> resp = service.getAuthUrl(req);
		System.out.println("resp:" + resp);
	}


	@Test
	public void getAuthUrl() {
		AuthReq req = new AuthReq();
		req.setUrlType("LOCAL");//ALIYUN
		req.setAttachid(1027069145561960448l);
		//单位秒
		req.setUrlExpire(3600l);
		req.setViewMode(ViewMode.VIEW);//图片可以直接在浏览器显示
		Response<String> response = service.getAuthUrl(req);
		System.out.println(response.getData());
	}
	
	@Test
	public void getAuthUrlKey() {
		AuthReq req = new AuthReq();
		req.setModulekey("CFBS");
		req.setAttachid(5l);
		//单位秒
		req.setUrlExpire(3600l);
		Response<String> response = service.getAuthUrl(req);
		System.out.println(JsonUtil.bean2Json(response));
	}
	
	@Test
	public void getAttach() {
		List<Long> list = new ArrayList<>();
		list.add(1002498158862536704l);
		Map<Long,AttachmentResp> response = service.getAttachment(list);
		System.out.println(JsonUtil.bean2Json(response));
	}

}
