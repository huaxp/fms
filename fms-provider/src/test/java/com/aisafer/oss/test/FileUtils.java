package com.aisafer.oss.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aisafer.fms.utils.UUId;

public final class FileUtils {

	private final Logger log = LoggerFactory.getLogger(FileUtils.class);

	private int indx = 10;
	
	public synchronized int nextIndx() {
		if (indx > 999)
			indx = 10;
		return indx++;
	}
	public final String newSerial() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmssSSS");
		return String.valueOf(nextIndx()).concat(sdf.format(new Date()) + new Random().nextInt(1000));
	}

	/**
	 * 接收并解压zip文件
	 * 用专门的unzip进行解压
	 * @param outputDirectory
	 * @param mfile
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	public String unzip(String outputDirectory, InputStream mfile) throws Exception {
		// 下载zip文件
		InputStream uploadStream = mfile;
		File uploadFile = null;
		ZipEntry zipEntry = null;
		String realpath = null;
		// 接收文件写到临时文件
		uploadFile = File.createTempFile("tmpzipfile" + newSerial(), ".zip");
		byte[] content = new byte[1024];

		FileOutputStream zos = new FileOutputStream(uploadFile);
		int dataLen;
		while ((dataLen = uploadStream.read(content)) > 0) {
			zos.write(content, 0, dataLen);
		}
		uploadStream.close();
		zos.flush();
		zos.close();

		// 解压
		FileOutputStream outputStream = null;
		ZipInputStream zipIs = new ZipInputStream(new FileInputStream(uploadFile));
		try {
			File unzipFile = new File(outputDirectory);
			if (!(unzipFile.exists())) {
				unzipFile.mkdirs();
			}
			realpath = outputDirectory + UUId.getUUid();
			File newfile = new File(realpath);
			if (!newfile.exists()) {
				newfile.mkdirs();
			}
			while ((zipEntry = zipIs.getNextEntry()) != null) {
				if (zipEntry.isDirectory()) {
					String name = zipEntry.getName();
					name = name.substring(0, name.length() - 1);
					File file = new File(realpath + "/" + name);
					file.mkdirs();
				} else {
					File file = new File(realpath + "/" + zipEntry.getName());
					file.createNewFile();
					outputStream = new FileOutputStream(file);
					// 普通解压缩文件
					int b;
					while ((b = zipIs.read()) != -1) {
						outputStream.write(b);
					}
					outputStream.flush();
					outputStream.close();
				}
			}
			zipIs.close();
			return realpath;
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new Exception("解压读取文件失败");
		} finally {

			IOUtils.closeQuietly(outputStream);
		}
	}

	public InputStream getFile(String filePath) {
		FileInputStream fis = null;
		try {
			File file = new File(filePath);
			fis = new FileInputStream(file);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return null;
		}
		return fis;
	}

	public byte[] getFilebyte(String filePath) {
		FileInputStream fis = null;
		try {
			File file = new File(filePath);
			fis = new FileInputStream(file);
			return IOUtils.toByteArray(fis);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return new byte[] {};
		}finally {
			IOUtils.closeQuietly(fis);
		}
	}

	public void byte2File(byte[] buf, String filePath, String fileName) throws IOException {
		try {
			File file = new File(filePath+fileName);	
			org.apache.commons.io.FileUtils.writeByteArrayToFile(file,buf);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}

	public void writeFile(InputStream in, String filePath, String fileName) throws IOException {
		try {
			File file = new File(filePath+fileName);	
			org.apache.commons.io.FileUtils.writeByteArrayToFile(file,IOUtils.toByteArray(in));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}
	
	public void writeFile(byte[] by, String filePath) throws IOException {
		try {
			File file = new File(filePath);	
			org.apache.commons.io.FileUtils.writeByteArrayToFile(file,by);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}
	
	public void writeFile(InputStream in, String filePath) throws IOException {
		try {
			File file = new File(filePath);	
			org.apache.commons.io.FileUtils.writeByteArrayToFile(file,IOUtils.toByteArray(in));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}

	public String  readTxtFile(String filePath) {
		FileInputStream fis = null;
		try {
			File file = new File(filePath);
			fis = new FileInputStream(file);
			return IOUtils.toString(fis,"UTF-8");
		} catch (Exception e) {
			log.error(e.getMessage());
			return "";
		}

	}

	public static FileUtils getFile() {
		return new FileUtils();
	}

}
