/*package com.aisafer.oss.test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.aisafer.ShenmaProviderApplication;
import com.aisafer.base.concurrent.ConcurrentFrame;
import com.aisafer.base.file.FileTypeJudge;
import com.aisafer.base.file.FileUtils;
import com.aisafer.base.utils.HttpUtil;
import com.aisafer.base.utils.UUId;
import com.aisafer.open.api.crypt.Base64;
import com.aisafer.oss.dro.AttachmentContent;
import com.aisafer.oss.dro.AttachmentReq;
import com.aisafer.oss.service.OSSService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShenmaProviderApplication.class)
public class OssCountDownLatchTest extends ConcurrentFrame{

	private static final Logger log = LoggerFactory.getLogger(OssCountDownLatchTest.class);
	
	@Autowired
	OSSService service;
	
	//String path = "d:/doc/互联网研发管理 V1.0(2).pdf";
	//String path = "d:/doc/teamworkUser.xml";
	String path = "/home/doc/10000001.pdf";
	String key = "";
	//String storagePath = "d:/file/";
	
	String storagePath = "/home/file/";


	@Test
	public void conTest() throws InterruptedException, ExecutionException {
		super.setConcurrentNum(50);
		super.setThreadNum(500);
		super.start();
	}
	
	public void updateFile() {
		AttachmentReq attReq = new AttachmentReq();
		attReq.setModuleKey("OSS");
		attReq.setSourceCode("10003");
		attReq.setUrlExpire(3600);
		AttachmentContent attachmentContent = null;
		attachmentContent = new AttachmentContent();
		attachmentContent.setContent("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n".getBytes());
		attachmentContent.setFileSize(Integer.valueOf(attachmentContent.getContent().length).longValue());
		attachmentContent.setFileType(".pdf");
		attachmentContent.setFileName(UUId.getUUid()+".pdf");
		attReq.addContent(attachmentContent);
		service.uploadFile(attReq);
	}

	@Override
	public void execute() {
		down();
	}
	
	public void down() {
		final Map<String, Object> content = new HashMap<String, Object>();
		content.put("attachid", "2328b92b84d8403782bbc58d318df99c");
		try {
			final String result = HttpUtil.post(getMap("pro")+"/download/1.0?", content);
			Assert.assertNotNull(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getMap(String key) {
		Map<String, String> map = new HashMap<>();
		map.put("dev", "http://127.0.0.1:8083/api/entry/");
		map.put("sit", "http://172.16.10.64:8094/oss/api/entry/");
		map.put("uat", "http://172.16.10.56:8083/oss/api/entry/");
		// map.put("sit", "http://139.224.8.8:9876/oss/api/entry/");
		map.put("perf", "http://172.16.10.5:8160/oss/api/entry/");
		map.put("devs", "http://192.168.10.240:4080/oss/api/entry/");
		map.put("pro", "http://oss-api.shenmajr.com/oss/api/entry/");
		return map.get(key);
	}

}
*/