/*package com.aisafer.oss.test;

import java.io.IOException;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.aisafer.ShenmaProviderApplication;
import com.aisafer.base.file.FileUtils;
import com.aisafer.oss.storage.IHdfs;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShenmaProviderApplication.class)
public class HDFSServiceTest {

	@Resource(name = "HDFSStorageManagerImpl")
	IHdfs service;
	
	//private String contentPath = "/home/stevin/json1.txt";
	private String contentPath = "D:/doc/1.png";
	private String fileKey = "20170223/1000000000000000000000000001/0093dbcc21ee840bda62581d3a5bfe5aa.png";
	
	@Test
	public void createFileTest() {
		service.upload(fileKey, FileUtils.getFile().getFilebyte(contentPath));
	}

	@Test
	public void uploadFileTest() {
		service.upload(contentPath, fileKey);
	}

	@Test
	public void renameTest() {
	}

	@Test
	public void deleteTest() {
		service.delete(fileKey);
	}

	@Test
	public void mkdirTest() {
	}
	
	@Test
	public void readFileTest() {
		byte[] str = service.readByte(fileKey);
		try {
			FileUtils.getFile().byte2File(str, "d:/file/", "hadoop_10007.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
*/