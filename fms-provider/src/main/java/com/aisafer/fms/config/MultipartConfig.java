package com.aisafer.fms.config;

import javax.servlet.MultipartConfigElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.aisafer.fms.config.AttachConfig;    
     
@Configuration    
@ComponentScan    
@EnableAutoConfiguration    
public class MultipartConfig {    
    
	@Autowired
	AttachConfig config;
	
    @Bean    
    public MultipartConfigElement multipartConfigElement() {    
        MultipartConfigFactory factory = new MultipartConfigFactory();    
        factory.setMaxFileSize(config.getMaxFileSize());    
        factory.setMaxRequestSize(config.getMaxRequestSize());    
        return factory.createMultipartConfig();    
    }    
     
}