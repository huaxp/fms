package com.aisafer.fms.action;
/*package com.aisafer.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.aisafer.base.enums.ResponseEnum;
import com.aisafer.base.pojo.Response;
import com.aisafer.open.api.crypt.AESCrypt2;
import com.aisafer.oss.dto.AttachmentContent;
import com.aisafer.oss.dto.AttachmentReq;
import com.aisafer.oss.dto.AttachmentResp;
import com.aisafer.oss.dto.AttachmentUploadReq;
import com.aisafer.oss.service.OSSService;
import com.aisafer.oss.service.impl.ModuleService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

*//**
 * 没有加密的文件信息
 *//*
@RestController
@RequestMapping("/api/entry")
public class SecurityAction {
	private static Logger logger = LoggerFactory.getLogger(SecurityAction.class);

	@Autowired
	private OSSService OSSService;

	@Autowired
	ModuleService moduleService;

	@RequestMapping(value = "/upload/v2/", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String uploadfile(HttpServletRequest request, HttpServletResponse response, AttachmentUploadReq req) {
		return saveFileMultipart(request, response, req);
	}

	private String saveFileMultipart(HttpServletRequest request, HttpServletResponse response,
			AttachmentUploadReq uploadParm) {
		ObjectMapper om = new ObjectMapper();
		try {
			AESCrypt2 aesCrypt2 = new AESCrypt2();
			MultipartHttpServletRequest mulReq = (MultipartHttpServletRequest) request;
			MultipartFile file = mulReq.getFile("file");
			AttachmentReq req = new AttachmentReq();
			AttachmentContent attachmentContent = new AttachmentContent();
			req.setModuleKey(uploadParm.getModuleKey());
			req.setSourceCode(uploadParm.getSourceCode());
			req.setUrlExpire(uploadParm.getUrlExpire());
			attachmentContent = new AttachmentContent();
			attachmentContent
					.setContent(aesCrypt2.decrypt(String.valueOf(request.getAttribute("dsk")), file.getBytes()));
			attachmentContent.setFileSize(Integer.valueOf(attachmentContent.getContent().length).longValue());
			attachmentContent.setFileType(uploadParm.getFileType());
			attachmentContent.setFileName(uploadParm.getFileName());
			attachmentContent.setCategoryType(uploadParm.getCategoryType());
			attachmentContent.setSourceSystem(uploadParm.getSourceSystem());
			req.addContent(attachmentContent);
			Response<AttachmentResp> result = OSSService.uploadFile(req);
			// RequestUtils.out(response, JSONUtil.toJson(result));
			return om.writeValueAsString(result);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Response<String> res = new Response<String>();
			res.setCode(ResponseEnum.FAILURE5010.getCode());
			res.setMsg(ResponseEnum.FAILURE5010.getMessage());
			try {
				logger.error(om.writeValueAsString(res));
				return om.writeValueAsString(res);
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
		}
		return null;
	}
}
*/