package com.aisafer.fms.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aisafer.base.multiple.RedisConfig;
import com.aisafer.base.multiple.RedisMultipleFactory;
import com.aisafer.fms.utils.UUId;

/**
 * 没有加密的文件信息
 */
@RestController
@RequestMapping("/attach")
public class TestAction {
	private static Logger logger = LoggerFactory.getLogger(TestAction.class);

	RedisMultipleFactory factory  = null;
	
	private static String uuid = "";

	@RequestMapping(value = "/put/v1/", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String put(HttpServletRequest request, HttpServletResponse response) {
		logger.info("token:{}",request.getParameter("token"));
		if(StringUtils.isEmpty(uuid)) {
			uuid = UUId.getUUid();
			logger.info("uuid:{}",uuid);
		}
		if(uuid.equals(request.getParameter("token"))) {
			getRedis();
			factory.put(request.getParameter("key"),request.getParameter("value"));
			return factory.get(request.getParameter("key"));
		}
		return "ok";
	}
	
	@RequestMapping(value = "/get/v1/", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String get(HttpServletRequest request, HttpServletResponse response) {
		logger.info("token:{}",request.getParameter("token"));
		if(StringUtils.isEmpty(uuid)) {
			uuid = UUId.getUUid();
			logger.info("uuid:{}",uuid);
		}
		if(uuid.equals(request.getParameter("token"))) {
			getRedis();
			return factory.get(request.getParameter("key"));
		}
		return "ok";
	}
	
	public void getRedis() {
		RedisConfig config = new RedisConfig();
		config.setDatabase(0);
		config.setEnable(true);
		config.setExpire(20000);
		config.setHostName("172.18.197.231:6379,172.18.197.236:6379,172.18.197.233:6379,172.18.197.231:6380,172.18.197.236:6380,172.18.197.233:6380");
		config.setLockExpire(20000);
		config.setMaxIdle(100);
		config.setMaxTotal(10);
		config.setMaxWaitMillis(20000);
		config.setPassword("Aisafer@1wDc2018");
		config.setSpin(false);
		config.setSpinNum(5);
		config.setSpinThreadTime(100);
		config.setTestOnBorrow(true);
		config.setTimeout(10000);
		factory  = new RedisMultipleFactory(config);
		factory.init();
	}

	
}
