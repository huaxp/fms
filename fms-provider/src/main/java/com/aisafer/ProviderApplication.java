package com.aisafer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;


@SpringBootApplication
@MapperScan("com.aisafer.fms.mapper")
@ImportResource(value={"classpath:/config/*.xml"})
public class ProviderApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ProviderApplication.class);
	}

	public static void main(String[] args) throws Exception {
		
		SpringApplication.run(ProviderApplication.class, args);
	}

}