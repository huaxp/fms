package com.aisafer.fms.mapper;

import org.springframework.stereotype.Repository;

import com.aisafer.fms.domain.StsParamConfig;

/**
 * 阿里云参数配置
 * @author stevin
 *
 */
@Repository
public interface StsParamConfigMapper {
    int deleteByPrimaryKey(String id);

    int insert(StsParamConfig record);

    int insertSelective(StsParamConfig record);

    StsParamConfig selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(StsParamConfig record);

    int updateByPrimaryKey(StsParamConfig record);
}