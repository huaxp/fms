package com.aisafer.fms.mapper;

import com.aisafer.fms.domain.StsConfig;

public interface StsConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(StsConfig record);

    int insertSelective(StsConfig record);

    StsConfig selectByPrimaryKey(Long id);
    
    StsConfig select(StsConfig stsConfig);

    int updateByPrimaryKeySelective(StsConfig record);

    int updateByPrimaryKey(StsConfig record);
}