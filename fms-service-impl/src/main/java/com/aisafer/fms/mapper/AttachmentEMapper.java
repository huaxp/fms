package com.aisafer.fms.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.aisafer.fms.domain.AttachmentE;

/**
 * 附件扩展Mapper
 * @author stevin
 */
//@CacheConfig(cacheNames="AttachmentEMapper") 
@Repository
public interface AttachmentEMapper {
    int deleteByPrimaryKey(String id);

    int insert(AttachmentE atta);
    
    int count(AttachmentE atta);

    int insertSelective(AttachmentE record);

    AttachmentE selectByPrimaryKey(String id);
    
    //@Cacheable(key= "#p0.attachid")
    
    List<AttachmentE> select(AttachmentE atta);

   // @CacheEvict(key="#p0.attachid") 
    int updateByPrimaryKeySelective(AttachmentE record);

    int updateByPrimaryKey(AttachmentE record);
    
    public List<AttachmentE> selectAttachGT(Map<String,Object> map);
    
    public List<AttachmentE> selectAttachLT(Map<String,Object> atta);
    
    public List<AttachmentE> selectAttachByattachid(List<Long> list);
    
    
}

