package com.aisafer.fms.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.aisafer.fms.domain.Module;

/**
 * 模块Mapper
 * @author stevin
 *
 */
@Repository
public interface ModuleMapper {
	
    int deleteByPrimaryKey(Long id);

    int insert(Module record);

    int insertSelective(Module record);

    Module selectByPrimaryKey(Long id);
    
    List<Module> findAll(Module module);
    
    Module select(Module module);

    int updateByPrimaryKeySelective(Module module);

    int updateByPrimaryKey(Module record);
}