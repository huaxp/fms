package com.aisafer.fms.mapper;

import java.util.List;

import com.aisafer.fms.domain.AttachmentGroup;

public interface AttachmentGroupMapper {
    int insert(AttachmentGroup record);
    
    List<AttachmentGroup> select(AttachmentGroup group);
    
    int insertBatch(List<AttachmentGroup> record);

    int insertSelective(AttachmentGroup record);
}