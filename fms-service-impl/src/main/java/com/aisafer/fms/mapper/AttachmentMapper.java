package com.aisafer.fms.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.aisafer.fms.domain.Attachment;
import com.aisafer.fms.dto.AttachmentHandleReq;

/**
 * 附件主表Mapper
 * @author stevin
 *
 */
//@CacheConfig(cacheNames="AttachmentMapper") 
@Repository
public interface AttachmentMapper  {
	
    int deleteByPrimaryKey(Long id);

    int insert(Attachment atta);

    int insertSelective(Attachment atta);
    
    //@Cacheable(key="#p0")
    Attachment selectByPrimaryKey(Long id);
    
    List<Attachment> select(Attachment atta);

    int updateByPrimaryKeySelective(Attachment atta);

    int updateByPrimaryKey(Attachment atta);
    
    List<Attachment> selectList(AttachmentHandleReq atta);
    
    
    List<Attachment> selectByList(List<Long> list);
    
}