package com.aisafer.fms.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.aisafer.base.enums.ResponseEnum;
import com.aisafer.base.enums.StatusEnum;
import com.aisafer.base.pojo.Response;
import com.aisafer.base.redis.RedisTools;
import com.aisafer.fms.client.dto.AttachmentAdditionalReq;
import com.aisafer.fms.client.dto.AttachmentContent;
import com.aisafer.fms.client.dto.AttachmentModifyReq;
import com.aisafer.fms.client.dto.AttachmentModifyResp;
import com.aisafer.fms.client.dto.AttachmentReq;
import com.aisafer.fms.client.dto.AttachmentResp;
import com.aisafer.fms.client.dto.AuthReq;
import com.aisafer.fms.client.dto.DeleteReq;
import com.aisafer.fms.client.dto.DownReq;
import com.aisafer.fms.client.dto.DownResp;
import com.aisafer.fms.client.dto.Order;
import com.aisafer.fms.client.enums.StorageTypeEnums;
import com.aisafer.fms.client.exception.ServiceException;
import com.aisafer.fms.config.AttachConfig;
import com.aisafer.fms.domain.Attachment;
import com.aisafer.fms.domain.AttachmentE;
import com.aisafer.fms.domain.Module;
import com.aisafer.fms.domain.OssMsg;
import com.aisafer.fms.enums.AttachmentEnums;
import com.aisafer.fms.enums.AttachmentStatusEnums;
import com.aisafer.fms.exception.StorageServiceException;
import com.aisafer.fms.mapper.AttachmentEMapper;
import com.aisafer.fms.mapper.AttachmentMapper;
import com.aisafer.fms.mapper.ModuleMapper;
import com.aisafer.fms.storage.StorageFactory;
import com.aisafer.fms.utils.AttachmentUtils;
import com.aisafer.fms.utils.DateUtil;
import com.aisafer.fms.utils.FileTypeJudge;
import com.aisafer.fms.utils.ImageUtil;
import com.aisafer.fms.utils.UUId;
import com.easy.mq.client.KafkaClient;
import com.polaris.base.generator.Generator;
import com.polaris.base.utils.date.DateUtils;
import com.polaris.base.utils.string.JsonUtil;


/**
 * 附件处理类
 * 
 * @author stevin
 *
 */
@Component("AttachmentServiceImpl")
public class AttachmentServiceImpl implements AttachmentService, com.aisafer.fms.client.service.AttachmentService {
	
	private static final Logger log = LoggerFactory.getLogger(AttachmentServiceImpl.class);
	@Autowired
	private ModuleMapper moduleDao;

	@Autowired
	private AttachmentMapper attachmentDao;

	@Autowired
	private AttachmentEMapper attachmentEDao;

	@Autowired
	private AttachConfig attachConfig;

	@Autowired
	private RedisTools redisTools;
	
	@Value("${fms.upload}")
	private String ossTopic;
	
	public  final ThreadLocal<String> local = new ThreadLocal<>();
	
	/**
	 * 附件上传入口
	 */
	@Override
	public Response<AttachmentResp> uploadFile(AttachmentReq req)  {
		try {
			long beginTime = System.currentTimeMillis();
			//log.info("oss:uploadFile");
			Response<AttachmentResp> response = new Response<AttachmentResp>();
			if(StringUtils.isEmpty(req.getModuleKey())) {
				response.setCode(ResponseEnum.FAILURE41004.getCode());
				response.setMessage(ResponseEnum.FAILURE41004.getMessage());
				log.error(JsonUtil.bean2Json(response));
				return response;
			}
			Module module = new Module();
			module.setModuleKey(req.getModuleKey());
			Module moduleObj = moduleDao.select(module);
			if (moduleObj == null || moduleObj.getId() <=0 ) {
				response.setCode(ResponseEnum.FAILURE41003.getCode());
				response.setMsg(ResponseEnum.FAILURE41003.getMessage());
				log.error(JsonUtil.bean2Json(response));
				return response;
			}
			if (StatusEnum.DISABLE.getCode().equals(moduleObj.getIsEnable())) {
				response.setCode(ResponseEnum.FAILURE41026.getCode());
				response.setMsg(ResponseEnum.FAILURE41026.getMessage());
				log.error(JsonUtil.bean2Json(response));
				return response;
			}

			updateFile(req, response, moduleObj);
			response.setCode(ResponseEnum.SUCESS200.getCode());
			response.setMsg(ResponseEnum.SUCESS200.getMessage());
			log.info("updateFile.total.time:{}", (System.currentTimeMillis() - beginTime));
			return response;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(e.getMessage()+"文件上传失败");
		}
	}
	
	@Override
	public Response<String> getAuthUrl(AuthReq req) throws StorageServiceException {
		long beginTime = System.currentTimeMillis();
		//req.setModulekey(getModuleKey(req).getModuleId());
		Map<String,String> map = new HashMap<>();
		if(StringUtils.isNotEmpty(req.getUrlType())) {
			for (String downOrder : StringUtils.split(attachConfig.getDownOrder(), ",")) {
				if(req.getUrlType().equalsIgnoreCase(downOrder)) {
					map.put(downOrder, StorageFactory.getInstance(downOrder).getAuthUrl(req));
				}
			}
		}else {
			for (String downOrder : StringUtils.split(attachConfig.getDownOrder(), ",")) {
				map.put(downOrder, StorageFactory.getInstance(downOrder).getAuthUrl(req));
			}
		}
		log.info("getAuthUrl.time:{} ", (System.currentTimeMillis() - beginTime));
		Response<String> response = new Response<String>();
		response.setCode(ResponseEnum.SUCESS200.getCode());
		response.setMsg(ResponseEnum.SUCESS200.getMessage());
		response.setData(JsonUtil.bean2Json(map));
		return response;
	}

	private void updateFile(AttachmentReq req, Response<AttachmentResp> response, Module moduleObj) throws ServiceException {

		try {
			//long beginTime = System.currentTimeMillis();
			if (req.getAttachmentContent() != null) {
				updateFile(req, response, moduleObj, req.getAttachmentContent());
			}
			//log.info("updateFile1.time:{}", (System.currentTimeMillis() - beginTime));
		} catch (Exception e) {
			log.error("msg:{}", e.getMessage(), e);
			throw new ServiceException("文件上传失败");
		}
	}
	
	private void updateFile(AttachmentReq req, Response<AttachmentResp> response, Module moduleObj,
			AttachmentContent content) {
		String storageKey = null;
		Attachment attach = new Attachment();
		final Long attachid = Generator.nextValue();
		try {
		
			setFileInfo(content, attach);
			storageKey = getStorageKey(req, content);
			//long beginTime2 = System.currentTimeMillis();
			saveAttach(attach, response, moduleObj, attachid, content, storageKey);
			//log.info("updateFile2.time:{}", (System.currentTimeMillis() - beginTime2));
			//long beginTime = System.currentTimeMillis();
			AttachmentE attachmentE = new AttachmentE();
			attachmentE.setAttachid(attachid);
			attachmentE.setStorageKey(storageKey);
			if(isMasterStorage(content)) {
				StorageFactory.getInstance(attachConfig.getMaster()).upload(attachmentE,content.getContent());
			}else if(StringUtils.isNotEmpty(content.getStorageKey()) && content.getStorageType() != null) {
				StorageFactory.getInstance(content.getStorageType().name()).upload(attachmentE,content.getContent());
			}
			//log.info("updateFile3.time:{}", (System.currentTimeMillis() - beginTime));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			boolean flag =false;
			// 循环把附件上传到服务器
			for (String slave : StringUtils.split(attachConfig.getSlave(), ",")) {
				try {
					AttachmentE attachmentE = new AttachmentE();
					attachmentE.setAttachid(attachid);
					attachmentE.setStorageKey(storageKey);
					StorageFactory.getInstance(slave).upload(attachmentE, content.getContent());
					flag = true;
					break;
				} catch (Exception e2) {
					flag = false;
					log.error(e.getMessage(), e);
				}
			}
			if(!flag) {
				response.setData(null);
				response.getDataList().clear();
				response.setCode(ResponseEnum.FAILURE5010.getCode());
				response.setMsg(ResponseEnum.FAILURE5010.getMessage());
				return;
			}
		}
		try {
			if(!"LOCAL".equals(local.get())) {
				OssMsg ossMsg = new OssMsg();
				ossMsg.setAttachid(attachid);
				ossMsg.setType(AttachmentEnums.UPLOAD.name());
				KafkaClient kafkaClient = ApplicationContextUtils.getBean("kafkaClient", KafkaClient.class);
				kafkaClient.send(ossTopic, ossMsg);
			}
			local.remove();
		} catch (Exception e) {
			log.error(e.getMessage() + "消息发送失败", e);
		}
	}

	private void setFileInfo(AttachmentContent content, Attachment attach) throws IOException {
		if(StringUtils.isEmpty(content.getFileType()) ||  content.getFileType().length() > 10) {
			final String fileType = String.valueOf(FileTypeJudge.getType(content.getContent()));
			if(StringUtils.isNotEmpty(fileType)) {
				content.setFileType(fileType);
			}
		}
		
		if((content.getFileSize() == null || content.getFileSize() <=0) && content.getContent() != null) {
			content.setFileSize(Integer.valueOf(content.getContent().length).longValue());
		}
		if(StringUtils.isNotEmpty(content.getFileType())&&content.getFileType().indexOf(".")<=-1) {
			attach.setFileType("."+content.getFileType());
		}else {
			attach.setFileType(content.getFileType());
		}
	}
	
	private boolean isMasterStorage(AttachmentContent content) {
		return content.getContent() != null && content.getContent().length > 0;
	}

	private String getStorageKey(AttachmentReq req, AttachmentContent content) {
		if(StringUtils.isNotEmpty(content.getStorageKey()) && (content.getContent() == null || content.getContent().length <=0)) {
			return content.getStorageKey();
		}
		StringBuilder sb = new StringBuilder();
		sb.append(DateUtil.getNowDate(DateUtil.DT_SHORT));
		sb.append("/");
		if(StringUtils.isNotEmpty(content.getSourceCode())) {
			sb.append(content.getSourceCode());
		}else {
			sb.append(String.valueOf(System.currentTimeMillis()));
		}
		sb.append("/");
		sb.append(getFileName(content, req));
		return String.valueOf(sb);
	}

	private String getFileName(AttachmentContent content, AttachmentReq req) {
		StringBuilder sb = new StringBuilder();
		sb.append(Generator.nextValue());
		if (StringUtils.isNotEmpty(content.getFileType()) && content.getFileType().indexOf(".") <= -1) {
			sb.append(".");
		}
		sb.append(content.getFileType());
		return String.valueOf(sb);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	private void saveAttach(final Attachment attach, final Response<AttachmentResp> response, final Module module,
			final Long attachid, final AttachmentContent content, final String storageKey) {
		attach.setCreateTime(DateUtils.getUTCDate());
		attach.setFileName(content.getFileName());
		attach.setId(attachid);
		attach.setModuleId(module.getId());
		attach.setLasttime(UUId.getUUIDLong());
		attach.setSourceCode(content.getSourceCode());
		if(content.getFileSize() != null && content.getFileSize() > 0) {
			attach.setFileSize(content.getFileSize().intValue());
		}
		attachmentDao.insert(attach);
		AttachmentResp resp = new AttachmentResp();
		resp.setAttachid(attachid);
		resp.setStorageKey(storageKey);
		//response.add(resp);
		response.setData(resp);
	}


	@Override
	public byte[] downloadFileByte(DownReq req) {
		DownResp down = null;
		try {

			long beginTime = System.currentTimeMillis();

			down = getAttachStream(req.getAttachid());
			// 如果attachid没有找到，那么就根据storageKey在下载一次
			if (down == null || down.getContent() == null || down.getContent().length <= 0) {
				byte[] content = getAttachStream(req).getContent();
				down.setContent(content);

			}
			log.info("attachid:{},size:{}", req.getAttachid(), down.getContent() != null ? down.getContent().length : 0);
			log.info("downloadFileByte.read.time:{} ", (System.currentTimeMillis() - beginTime));
			
			if(req.getHeight() != null && req.getWidth() != null && req.getHeight() > 0 && req.getWidth() > 0) {
				try {
					long beginTime3 = System.currentTimeMillis();
					byte [] content  = new ImageUtil().scale(down.getContent(), req.getHeight(), req.getWidth(), true);
					log.info("downloadFileByte.scale.time:{} ", (System.currentTimeMillis() - beginTime3));
					return content;
				}catch(Exception e) {
					log.error(e.getMessage());
				}
			}
			return down.getContent();
		} catch (Exception e) {
			log.error("filecontent:{},msg:{}", e.getMessage(), e);
			return new byte[] {};
		}
	}
	
	@Override
	public DownResp download(DownReq req) throws ServiceException {
		long beginTime = System.currentTimeMillis();
		byte[] content = this.downloadFileByte(req);
		Attachment attachment = this.attachmentDao.selectByPrimaryKey(req.getAttachid());
		DownResp resp = new DownResp();
		resp.setContent(content);
		if (attachment != null) {
			resp.setFileName(attachment.getFileName());
			resp.setFileType(attachment.getFileType());
		}
		log.info("download.read.time:{} ", (System.currentTimeMillis() - beginTime));
		return resp;
	}

	@Override
	public List<AttachmentE> getAttachment(String storageType) {
		AttachmentE attaE = new AttachmentE();
		attaE.setStorageType(storageType);
		return attachmentEDao.select(attaE);
	}

	
	@Override
	public DownResp getAttachStream(Long attachid) {
		
		AttachmentE attaE = null;
		DownResp down = new DownResp();
		byte[] is = null;
		if(attachid <= 0) {
			return down;
		}
		DownReq downReq = null;
		for (String downOrder : StringUtils.split(attachConfig.getDownOrder(), ",")) {
			try {
				attaE = new AttachmentE();
				attaE.setAttachid(attachid);
				attaE.setStorageType(downOrder);
				attaE.setStatus(AttachmentStatusEnums.SUCCESS.getName());
				List<AttachmentE> attachmentList = attachmentEDao.select(attaE);
				if (attachmentList == null || attachmentList.size() <= 0) {
					log.warn("storageType:{},attachid:{},msg:{}", downOrder, attachid, "获取文件信息为空");
					continue;
				}
				AttachmentE attachmentE = attachmentList.get(0);
				downReq = new DownReq();
				downReq.setAttachid(attachmentE.getAttachid());
				downReq.setStorageKey(attachmentE.getStorageKey());
				is = getStream(downReq, downOrder);
				if (is == null || is.length <=0) {
					log.warn("storageType:{},attachid:{},msg:{}", downOrder, attachid, "获取文件为空");
					continue;
				}

				down.setFileName(attachmentE.getStorageKey());
				down.setContent(is);
				log.info("attachid:{},storageType:{},size:{}", attachid, downOrder, down.getContent().length);
				return down;
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return down;
	}

	public DownResp getAttachStream(DownReq req) {
		DownResp down = new DownResp();
		if(req.getAttachid() <=0) {
			return down;
		}
		byte[] is = null;
		for (String downOrder : split(attachConfig.getDownOrder(), ",")) {
			try {
				is = getStream(req, downOrder);
				if (is == null) {
					continue;
				}
				down.setFileName(String.valueOf(req.getAttachid()));
				down.setContent(is);
				log.info("attachid:{},storageType:{},size:{}", req.getAttachid(), downOrder, down.getContent().length);
				return down;
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return down;
	}
	
	private  List<String> split(String str, String regex) {
		if (str.indexOf(regex) <= -1) {
			return Arrays.asList(new String[] { str });
		}
		return Arrays.asList(str.split(regex));
	}

	private byte[] getStream(DownReq req, String order) {
		return StorageFactory.getInstance(order).read(req);
	}

	@Override
	public List<AttachmentE> getAttachment(Long attachid, String storageType) {
		List<AttachmentE> atteList = null;
		AttachmentE attaParam = null;
		attaParam = new AttachmentE();
		attaParam.setAttachid(attachid);
		attaParam.setStorageType(storageType);
		atteList = attachmentEDao.select(attaParam);
		if (atteList == null || atteList.size() <= 0) {
			attaParam = new AttachmentE();
			attaParam.setStorageKey(String.valueOf(attachid));
			attaParam.setStorageType(storageType);
			atteList = attachmentEDao.select(attaParam);
		}
		return atteList;
	}

	@Override
	public void saveAttach(AttachmentE attachmentE, StorageTypeEnums storage, AttachmentStatusEnums status) throws ServiceException {

		if (redisTools.tryLock(attachmentE.getAttachid() + storage.name())) {
			try {
				AttachmentE attaE = new AttachmentE();
				attaE.setAttachid(attachmentE.getAttachid());
				attaE.setStorageType(storage.name());
				Integer countAtta = attachmentEDao.count(attaE);
				if (countAtta == null || countAtta <= 0) {
					AttachmentE atte = new AttachmentE();
					atte.setId(Generator.nextValue());
					atte.setAttachid(attachmentE.getAttachid());
					atte.setStorageKey(attachmentE.getStorageKey());
					if(attachmentE.getCreateTime() != null) {
						atte.setCreateTime(attachmentE.getCreateTime());
					}else {
						atte.setCreateTime(DateUtils.getUTCDate());
					}
					atte.setUpdateTime(DateUtils.getUTCDate());
					atte.setTag(attachmentE.getTag());
					atte.setStorageType(storage.name());
					atte.setStatus(status.getName());
					atte.setLasttime(UUId.getUUIDLong());
					attachmentEDao.insert(atte);
				} /*else {
					List<AttachmentE> attachmentList = attachmentEDao.select(attaE);
					if (attachmentList != null && attachmentList.size() > 0) {
						AttachmentE atta = attachmentList.get(0);
						atta.setUpdateTime(DateUtils.getUTCDate());
						atta.setTag(attachmentE.getTag());
						atta.setStorageKey(attachmentE.getStorageKey());
						atta.setStatus(status.getName());
						attachmentEDao.updateByPrimaryKey(atta);
					}
				}*/
			} finally {
				redisTools.unLock(attachmentE.getAttachid() + storage.name());
			}
		} else {
			throw new ServiceException("存在重复操作" + attachmentE.getAttachid() + storage);
		}
	}

	@Override
	public Response<AttachmentModifyResp> attachmentModify(AttachmentModifyReq req) {
		Response<AttachmentModifyResp> resp = new Response<AttachmentModifyResp>();
		Attachment attachment = new Attachment();
		attachment.setId(req.getAttachid());
		Integer result = this.attachmentDao.updateByPrimaryKeySelective(attachment);
		if(result != null && result > 0) {
			AttachmentModifyResp attachmentResp = new AttachmentModifyResp();
			attachmentResp.setAttachid(req.getAttachid());
			resp.setCode(ResponseEnum.SUCESS200.getCode());
			resp.setMsg(ResponseEnum.SUCESS200.getMessage());
			return resp;
			
		}
		resp.setCode(ResponseEnum.FAILURE500.getCode());
		resp.setMsg("更新失败："+req.getAttachid()+"");
		return resp;
	}

	@Override
	public Response<AttachmentResp> uploadAdditionalFile(AttachmentAdditionalReq attachReq)
			throws com.aisafer.base.exception.ServiceException {
		attachReq.setAttachmentContent(attachReq.getAttachmentAdditionalContent());
		local.set("LOCAL");
		Response<AttachmentResp> resp =  this.uploadFile(attachReq);
		List<Order>	 list = redisTools.getList(attachReq.getAttachmentAdditionalContent().getBatchid(),Order.class);
		
		if(list == null || list.size() <=0) {
			list = new ArrayList<>();
		}
		if(resp.getData() == null) {
			return resp;
		}
		Order order = attachReq.getAttachmentAdditionalContent().getOrder();
		order.setAttachid(resp.getData().getAttachid());
		order.setStorageKey(resp.getData().getStorageKey());
		if(order.getSize() <=0) {
			order.setSize(attachReq.getAttachmentContent().getContent().length);
		}
		addOrder(list, order);
		redisTools.put(attachReq.getAttachmentAdditionalContent().getBatchid(), list);
		
		return resp;
	}
	
	private void addOrder(List<Order>	 list,Order order) {
		list.add(order);
	}

	@Override
	public Response<AttachmentResp> uploadMergeFile(AttachmentAdditionalReq attachReq)
			throws com.aisafer.base.exception.ServiceException {
		long begin = System.currentTimeMillis();
		log.info("uploadMergeFile:batchid {}",attachReq.getAttachmentAdditionalContent().getBatchid());
		List<Order>	 list = redisTools.getList(attachReq.getAttachmentAdditionalContent().getBatchid(),Order.class);
		log.info("uploadMergeFile.redis.size:{}",list != null ? list.size() : 0);
		if(list == null || list.size() <=0) {
			Response<AttachmentResp> resp = new Response<>();
			return resp;
		}
		sort(list);
		//int totalSize = sumSize(list);
		//byte[] content = new byte[totalSize];
		byte [] content = null;
		DownReq downReq = null;
		for(Order order : list) {
			downReq = new DownReq();
			downReq.setAttachid(order.getAttachid());
			downReq.setStorageKey(order.getStorageKey());
			//byte[] downByte = downloadFileByte(downReq);
			byte[] downByte = getStream(downReq, "LOCAL");
			if(downByte == null) {
				continue;
			}
			if(content == null) {
				content = downByte;
			}else {
				content = byteMerger(content, downByte);
			}
		}
		
		attachReq.setAttachmentContent(attachReq.getAttachmentAdditionalContent());
		attachReq.getAttachmentContent().setContent(content);
		Response<AttachmentResp> resp =  uploadFile(attachReq);
		log.info("uploadMergeFile:batchid {},attachid {},time {}",attachReq.getAttachmentAdditionalContent().getBatchid(),resp.getData().getAttachid(),(System.currentTimeMillis()-begin));
		return resp;
	}
	
	private  byte[] byteMerger(byte[] byte_1, byte[] byte_2){  
        byte[] byte_3 = new byte[byte_1.length+byte_2.length];  
        System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);  
        System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);  
        return byte_3;  
    }  
	
	private void sort(List<Order>list) {
		 // 按点击数倒序
        Collections.sort(list, new Comparator<Order>() {
            public int compare(Order arg0, Order arg1) {
                int hits0 = arg0.getOrder();
                int hits1 = arg1.getOrder();
                if (hits1 > hits0) {
                    return -1;
                } else if (hits1 == hits0) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
	}

	@Override
	public Response<AttachmentResp> uploadRollbackAdditionalFile(String batchid)
			throws com.aisafer.base.exception.ServiceException {
		Response<AttachmentResp> response = new Response<AttachmentResp>();
		OssMsg msg = new OssMsg();
		msg.setType(AttachmentEnums.CLEAN.name());
		msg.setBatchid(batchid);
		KafkaClient kafkaClient = ApplicationContextUtils.getBean("kafkaClient", KafkaClient.class);
		kafkaClient.send(ossTopic, msg);
		response.setCode(ResponseEnum.SUCESS200.getCode());
		return response;
	}

	@Override
	public void cleanGroupFile(String batchid) {
		List<Order>	 list = redisTools.getList(batchid,Order.class);
		if(list == null || list.size() <=0) {
			return;
		}
		DeleteReq req = null;
		for(Order order : list) {
			req = new DeleteReq();
			req.setAttachid(order.getAttachid());
			req.setStorageKey(order.getStorageKey());
			for (String storageType : AttachmentUtils.split(attachConfig.getSlave(), ",")) {
				StorageFactory.getInstance(storageType).delete(req);
			}
		}
		redisTools.delete(batchid);
	}

	@Override
	public Map<Long, AttachmentResp> getAttachment(List<Long> list) {
		Map<Long,AttachmentResp> map = new HashMap<>();
		List<Attachment> attachmentList =  attachmentDao.selectByList(list);
		AttachmentResp resp = null;
		for(Attachment attachment : attachmentList) {
			resp = new AttachmentResp();
			resp.setAttachid(attachment.getId());
			resp.setFileType(attachment.getFileType());
			resp.setSize(attachment.getFileSize());
			map.put(attachment.getId(), resp);
		}
		return map;
	}

	

}
