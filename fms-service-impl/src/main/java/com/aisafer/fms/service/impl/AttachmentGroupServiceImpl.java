package com.aisafer.fms.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aisafer.base.enums.ResponseEnum;
import com.aisafer.base.pojo.Response;
import com.aisafer.fms.client.dto.AttachmentGroupReq;
import com.aisafer.fms.client.dto.AttachmentGroupResp;
import com.aisafer.fms.client.service.AttachmentGroupService;
import com.aisafer.fms.domain.AttachmentGroup;
import com.aisafer.fms.mapper.AttachmentGroupMapper;
import com.polaris.base.generator.Generator;

@Service("attachmentGroupService")
public class AttachmentGroupServiceImpl implements AttachmentGroupService {
	
	private static final Logger log = LoggerFactory.getLogger(AttachmentGroupServiceImpl.class);
	
	@Autowired
	AttachmentGroupMapper attachmentGroupMapper;
	
	@Autowired
	AttachmentService attachmentService;
	
	@Override
	public Response<AttachmentGroupResp> insertGroup(List<AttachmentGroupReq> req) {
		long begin = System.currentTimeMillis();
		Response<AttachmentGroupResp> resp = new Response<>();
		List<AttachmentGroup> listGroup= new ArrayList<>();
		AttachmentGroup attachmentGroup = null;
		final Date date = new Date();
		
		for(AttachmentGroupReq group : req) {
			attachmentGroup = new AttachmentGroup();
			attachmentGroup.setAttachid(group.getAttachid());
			attachmentGroup.setCreateTime(date);
			attachmentGroup.setUpdateTime(date);
			attachmentGroup.setFileBlock(group.getFileBlock());
			attachmentGroup.setGroupid(group.getGroupid());
			attachmentGroup.setId(Generator.nextValue());
			attachmentGroup.setLasttime(date.getTime());
			listGroup.add(attachmentGroup);
		}
		attachmentGroupMapper.insertBatch(listGroup);
		log.info("insertGroup.time:{}",(System.currentTimeMillis()-begin));
		resp.setCode(ResponseEnum.SUCESS200.getCode());
		return resp;
	}

	@Override
	public List<AttachmentGroupResp> getList(AttachmentGroupReq req) {
		AttachmentGroup group = new AttachmentGroup();
		group.setGroupid(req.getGroupid());
		List<AttachmentGroup> result =  attachmentGroupMapper.select(group);
		List<AttachmentGroupResp> respList = new ArrayList<>();
		AttachmentGroupResp attachmentGroupResp = null;
		for(AttachmentGroup attachmentGroup : result) {
			attachmentGroupResp = new AttachmentGroupResp();
			attachmentGroupResp.setGroupid(attachmentGroup.getGroupid());
			attachmentGroupResp.setAttachid(attachmentGroup.getAttachid());
			attachmentGroupResp.setFileBlock(attachmentGroup.getFileBlock().intValue());
			respList.add(attachmentGroupResp);
		}
		return respList;
	}

}
