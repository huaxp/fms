package com.aisafer.fms.service.impl;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author stevin
 */
@Component
public class ApplicationContextUtils implements ApplicationContextAware {
	
	
	private static ApplicationContext context = null;
	
	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		ApplicationContextUtils.context = context;
	}
	
	public static void setContext(ApplicationContext context){
		setContext(context);
	}
	
	/**
	 * @param beanName
	 * @param clazz
	 * @return
	 */
	public static <T> T getBean(String beanName,Class<T> clazz){
		T obj = (T) context.getBean(beanName, clazz);
		return obj;
	}
	
    public static Object getBean(String name){
        return context.getBean(name);
    }
}
