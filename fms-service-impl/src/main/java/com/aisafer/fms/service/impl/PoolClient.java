package com.aisafer.fms.service.impl;

import java.util.Date;

/**
 * @author stevin
 */
public class PoolClient<T> {
	private boolean isUse;
	private T  client;
	private Date createDate;
	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	private Date updateDate;
	private String key;
	
	public PoolClient(boolean isUse, T client, Date createDate, Date updateDate) {
		this.isUse = isUse;
		this.client = client;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}


	public boolean isUse() {
		return isUse;
	}


	public void setUse(boolean isUse) {
		this.isUse = isUse;
	}


	public T getClient() {
		return client;
	}


	public void setClient(T client) {
		this.client = client;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}
	
	
}
