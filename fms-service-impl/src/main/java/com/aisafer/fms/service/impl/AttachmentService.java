package com.aisafer.fms.service.impl;

import java.util.List;

import com.aisafer.fms.client.dto.DownResp;
import com.aisafer.fms.client.enums.StorageTypeEnums;
import com.aisafer.fms.client.exception.ServiceException;
import com.aisafer.fms.domain.AttachmentE;
import com.aisafer.fms.enums.AttachmentStatusEnums;

/**
 * 附件接口
 * @author stevin
 *
 */
public interface AttachmentService {
	/**
	 * 根据存储类型获取附件信息
	 * @param storageType
	 * @return
	 */
	List<AttachmentE> getAttachment(String storageType);
	
	
	/**
	 * 根据存储类型获取附件信息
	 * @param storageType
	 * @return
	 */
	List<AttachmentE> getAttachment(Long attachid,String storageType);
	/**
	 * 根据attachid获取附件信息
	 * @param attachid
	 * @return
	 */
	DownResp  getAttachStream(Long attachid);
	/**
	 * 保存附件信息
	 */
	public void saveAttach(AttachmentE attachmentE,StorageTypeEnums storage,AttachmentStatusEnums status) throws ServiceException;
	
	public void cleanGroupFile(String batchid);
}
