package com.aisafer.fms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aisafer.fms.client.dto.ModuleReq;
import com.aisafer.fms.domain.Module;
import com.aisafer.fms.mapper.ModuleMapper;

/**
 * 模块实现服务
 * @author stevin
 *
 */
@Service("moduleService")
public class ModuleServiceImpl implements ModuleService{

	@Autowired
	ModuleMapper moduleDao;
	
	@Override
	public Module find(ModuleReq req) {
		Module param = new Module();	
		param.setModuleKey(req.getModuleKey());
		Module module =  moduleDao.select(param);
		return module;
	}

}
