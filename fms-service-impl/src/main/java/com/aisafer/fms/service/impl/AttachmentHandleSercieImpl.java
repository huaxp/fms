package com.aisafer.fms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aisafer.fms.domain.Attachment;
import com.aisafer.fms.dto.AttachmentHandleReq;
import com.aisafer.fms.mapper.AttachmentMapper;

@Service
public class AttachmentHandleSercieImpl implements AttachmentHandleSercie {

	@Autowired
	AttachmentMapper attachmentMapper;

	@Override
	public List<Attachment> getAttachmentList(AttachmentHandleReq req) {

		/*if (req.getPage() != null && req.getRows() != null) {
			PageHelper.startPage(req.getPage(), req.getRows());
		}*/

		List<Attachment> list =  attachmentMapper.selectList(req);
		return list;
	}

}
