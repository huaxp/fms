package com.aisafer.fms.service;

import com.aisafer.fms.client.dto.AuthReq;
import com.aisafer.fms.client.dto.DeleteReq;
import com.aisafer.fms.client.dto.DownReq;
import com.aisafer.fms.domain.AttachmentE;

public interface IStorageManager {

		public void upload(AttachmentE attachmentE,byte[] content);
		
		public byte[] read(DownReq atte);
		
		//public byte[] read(DownReq req)   ;
		
		public String getAuthUrl(AuthReq req) ;
		
		public boolean delete(DeleteReq req);
		
		
}
