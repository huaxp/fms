package com.aisafer.fms.service.impl;

import com.aisafer.fms.client.dto.ModuleReq;
import com.aisafer.fms.domain.Module;

/**
 * 模块服务接口
 * @author stevin
 *
 */
public interface ModuleService {
		/**
		 * 查找模块
		 * @param req
		 * @return
		 */
		public Module find(ModuleReq req);
		
}
