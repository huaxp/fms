package com.aisafer.fms.service.impl;

import java.util.List;

import com.aisafer.fms.domain.Attachment;
import com.aisafer.fms.dto.AttachmentHandleReq;

public interface AttachmentHandleSercie {
	
	public List<Attachment> getAttachmentList(AttachmentHandleReq req);
}
