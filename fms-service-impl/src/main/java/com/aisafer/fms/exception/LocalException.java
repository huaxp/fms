package com.aisafer.fms.exception;

public class LocalException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3338945095699568333L;

	public LocalException(String message) {
		super(message);
	}

	
}
