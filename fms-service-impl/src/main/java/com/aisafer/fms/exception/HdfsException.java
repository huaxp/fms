package com.aisafer.fms.exception;

public class HdfsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3338945095699568333L;

	public HdfsException(String message) {
		super(message);
	}

	
}
