package com.aisafer.fms.exception;

public class StorageServiceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3338945095699568333L;

	public StorageServiceException(String message) {
		super(message);
	}

	
}
