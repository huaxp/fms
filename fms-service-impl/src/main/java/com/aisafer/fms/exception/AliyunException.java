package com.aisafer.fms.exception;

/**
 * 异常
 * @author stevin
 */
public class AliyunException extends RuntimeException {

	
	private static final long serialVersionUID = 3338945095699568333L;

	public AliyunException(String message) {
		super(message);
	}

	
}
