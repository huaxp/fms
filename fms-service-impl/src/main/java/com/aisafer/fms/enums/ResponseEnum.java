
 package com.aisafer.fms.enums;

public enum ResponseEnum
 {
	 SUCESS200("200", "成功"), 
	 PROCESS31001("31001", "验签通过"), 
	 FAILURE41001("41001", "签名验证不通过"), 
	 FAILURE41002("41002", "访问接口失败"), 
	 FAILURE41003("41003", "校验不通过"), 
	 FAILURE41004("41004", "商户不存在"), 
	 FAILURE41005("41005", "没有对应接口"), 
	 FAILURE41006("41006", "协议版本不支持"), 
	 FAILURE41007("41007", "请求已过期"), 
	 FAILURE41008("41008", "不支持的签名加密"), 
	 FAILURE41009("41009", "没有收到参数"), 
	 FAILURE41010("41010", "没有该业务接口访问权限"),
	 FAILURE41011("41011", "没有收到交易编号"),
	 FAILURE41012("41012", "交易编号重复"),
	 FAILURE41013("41013", "不能重复支付"),
	 FAILURE41014("41014", "订单未找到"),
	 FAILURE41015("41015", "申请金额不能小于500元"),
	 FAILURE41016("41016", "未找到匹配数据"),
	 FAILURE41025("41025", "额度为空"),
	 FAILURE41017("41017", "额度不足"),
	 FAILURE41018("41018", "经销商资金账户已经冻结,请联系客服"),
	 FAILURE41019("41019", "申请金额不可大于可用额度"),
	 FAILURE41024("41024", "已用额度大于授信额"),
	 FAILURE41020("41020", "额度未评估,请联系客服"),
	 FAILURE41021("41021", "不支持运算类型"),
	 FAILURE41022("41022", "额度不足"),
	 FAILURE41023("41023", "贷款金额大于限价金额"),
	 FAILURE41026("41026", "该操作已停用"),
	 FAILURE1001("1001", "用户名重复"),
	 FAILURE1004("1004", "手机号重复"),
	 FAILURE1005("1005", "账号重复"),
	 FAILURE2001("2001", "账号不存在"),
	 FAILURE2002("2002", "账号已被禁用"),
	 FAILURE2003("2003", "账号已存在"),
	 FAILURE3001("3001", "用户名不能为空"),
	 FAILURE3002("3002", "密码不能为空"),
	 FAILURE3003("3003", "账号不能为空"),
	 FAILURE3004("3004", "手机号不能为空"),
	 FAILURE3005("3005", "密码错误"),
	 FAILURE403("403", "非法操作"),
	 FAILURE5001("5001", "会话不存在或过期，请重新登陆"),
	 FAILURE6002("6002", "请使用已绑定的手机登录"),
	 FAILURE6003("6003", "设备ID不能为空"),
	 FAILURE6004("6004", "验证码不正确"),
	 
	 FAILURE91001("91001", "获取地址信息为空"),
	 FAILURE91002("91002", "解析地址传入参数不能为空"),
	 FAILURE91003("91003", "解析地址失败,暂无匹配地址"),
	 FAILURE500("500", "系统异常"),
	 FAILURE5010("5010", "上传文件异常"),
		
	 ERROR("err", "系统异常");
	 
	
   private String code;
   private String message;
 
   private ResponseEnum(String code, String message) {
	   this.code = code;
	   this.message = message;
   }
 
   public String getCode() {
     return this.code;
   }
 
   public void setCode(String code) {
     this.code = code;
   }

  public String getMessage() {
     return this.message;
   }
 
   public void setMessage(String message) {
    this.message = message;
   }
 
   public static boolean isVerificated(String code)
   {
     if (code == null) {
       return false;
     }
     return PROCESS31001.getCode().equals(code);
   }
   public static boolean isSuccess(String code)
   {
     if (code == null) {
       return false;
   }
     return SUCESS200.getCode().equals(code);
   }
 }
