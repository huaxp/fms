package com.aisafer.fms.enums;

public enum AttachmentStatusEnums {
	
	SUCCESS("S"),FAIL("F"),DELETE("D");
	
	private String name;
	AttachmentStatusEnums(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
