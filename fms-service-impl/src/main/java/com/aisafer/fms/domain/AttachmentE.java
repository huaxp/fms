package com.aisafer.fms.domain;

import java.io.Serializable;
import java.util.Date;

public class AttachmentE implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -8661708652138835012L;

	private Long id;

    private Long attachid;

    private String storageType;

    private String storageKey;

    private String tag;

    private String status;

    private Date createTime;

    private Date updateTime;
    
    private Long lasttime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAttachid() {
		return attachid;
	}

	public void setAttachid(Long attachid) {
		this.attachid = attachid;
	}

	public String getStorageType() {
		return storageType;
	}

	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	public String getStorageKey() {
		return storageKey;
	}

	public void setStorageKey(String storageKey) {
		this.storageKey = storageKey;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getLasttime() {
		return lasttime;
	}

	public void setLasttime(Long lasttime) {
		this.lasttime = lasttime;
	}

  
}