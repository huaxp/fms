package com.aisafer.fms.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class AttachmentUtils {
	public  static List<String> split(String str, String regex) {
		if (str.indexOf(regex) <= -1) {
			return Arrays.asList(new String[] { str });
		}
		return Arrays.asList(str.split(regex));
	}
	
	public static String mapConvertUrl(Map<String, Object> param) {
		StringBuilder sb = new StringBuilder();
		try {
			for (Map.Entry<String, Object> entry : param.entrySet()) {
				sb.append(entry.getKey() + "=");

				sb.append(URLEncoder.encode(String.valueOf(entry.getValue()), "UTF-8"));

				sb.append("&");
			}
		} catch (UnsupportedEncodingException e) {
			return "";
		}
		if (sb.toString().endsWith("&")) {
			sb.delete(sb.length() - 1, sb.length());
		}
		return String.valueOf(sb);
	}
}
