package com.aisafer.fms.utils;

import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class UUId {
	
	private  Lock lock = new ReentrantLock();
	
	private   Long num = 0l;
	
	public static UUId uuid = new UUId();
	
	public static UUId getUUID() {
		return uuid;
	}

	public  static String getUUid() {
		return UUID.randomUUID().toString().replace("-", "").toLowerCase();
	}
	
	public  long getAutoIncrementLong(Long initValue) {
		lock.lock();
		try {
			if(num < 0) {
				num = initValue;
			}
			return num++;
		}finally {
			lock.unlock();
		}
	}

	public static synchronized  long getUUIDLong() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.currentTimeMillis());
		sb.append(Thread.currentThread().getId());
		return Long.parseLong(sb.toString());
	}
}
