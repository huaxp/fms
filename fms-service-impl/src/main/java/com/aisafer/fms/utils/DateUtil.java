/**
 * shenmajr.com Inc.
 * Copyright (c) 2015-2015 All Rights Reserved.
 */
package com.aisafer.fms.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日期工具类
 * 
 * @author Louis Lau
 * @version $Id: DateUtil.java, v 0.1 2015年7月10日 下午2:19:39 Louis Lau Exp $
 */
public class DateUtil {

	private final static Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);

	public static final String SIMPLE = "yyyy-MM-dd HH:mm:ss";

	public static final String SIMPLE_FULL = "yyyy-MM-dd HH:mm:ss:SSS";

	public static final String DT_SIMPLE = "yyyy-MM-dd";

	public static final String DT_SIMPLE_MONTH = "yyyy-MM";

	public static final String DT_SIMPLE_CHINESE = "yyyy年MM月dd日";

	public static final String DT_SHORT = "yyyyMMdd";

	public static final String DT_LONG = "yyyyMMddHHmmss";

	public static final String DT_SIXTEEN_BIT = "yyyyMMddHHmm";

	public static final String HMS_FORMAT = "HH:mm:ss";

	public static final String SIMPLE_FORMAT = "yyyy-MM-dd HH:mm";

	public static final String MD_FORMAT = "MMdd";

	public static final String DT_YYYYMM = "yyyyMM";

	/** 5分钟的毫秒数 */
	public static final long MILLISECOND = 300000L;

	private static final DateFormat getFormat(String format) {
		return new SimpleDateFormat(format);
	}

	public static final String fullDate(Date Date) {
		if (Date == null) {
			return null;
		}
		return getFormat(SIMPLE_FULL).format(Date);
	}

	/**
	 * yyyy-MM-dd HH:mm:ss
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static final String simpleFormat(Date date) {
		if (date == null) {
			return "";
		}

		return getFormat(SIMPLE).format(date);
	}

	/**
	 * 根据类型格式化日期
	 * 
	 * @param date
	 * @param format
	 * @return
	 */
	public static final String formatDateString(Date date, String format) {
		if (date == null || StringUtils.isEmpty(format)) {
			return "";
		}

		return getFormat(format).format(date);
	}

	/**
	 * yyyy-MM-dd
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static final String dtSimpleFormat(Date date) {
		if (date == null) {
			return "";
		}
		return getFormat(DT_SIMPLE).format(date);
	}

	/**
	 * yyyy-MM
	 * 
	 * @param date
	 * @return
	 */
	public static final String dtSimpleMonthFormat(Date date) {
		if (date == null) {
			return "";
		}
		return getFormat(DT_SIMPLE_MONTH).format(date);
	}

	/**
	 * 获取下一个月份 格式:yyyy-MM
	 * 
	 * @return
	 */
	public static final String dtSimpleNextMonthFormat() {

		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 2;
		if (month > 12) {
			year = year + 1;
			month = 1;
		}
		if (month < 10) {
			return year + "-0" + month;
		}
		return year + "-" + month;
	}

	/**
	 * yyyy-mm-dd 日期格式转换为日期
	 * 
	 * @param strDate
	 * 
	 * @return
	 */
	public static final Date strToDtSimpleFormat(String strDate) {
		if (strDate == null) {
			return null;
		}

		try {
			return getFormat(DT_SIMPLE).parse(strDate);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static final Date strToDtSimpleFormat(String strDate, String format) {
		if (strDate == null) {
			return null;
		}

		try {
			return getFormat(format).parse(strDate);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 日期格式化 输出 yyyy-MM-dd格式的Date类型 2016年3月3日 下午4:28:46 Louis Lau
	 * 
	 * @param today
	 * @return
	 */
	public static final Date todayFormat(Date today) {
		return strToDtSimpleFormat(dtSimpleFormat(today));
	}

	/**
	 * yyyy-MM-dd HH:mm 日期格式转换为日期
	 * 
	 * @param strDate
	 * 
	 * @return
	 */
	public static final Date strToSimpleFormat(String strDate) {
		if (strDate == null) {
			return null;
		}

		try {
			return getFormat(SIMPLE_FORMAT).parse(strDate);

		} catch (Exception e) {
			;
		}

		return null;
	}

	/**
	 * yyyy-MM-dd HH:mm 或者yyyy-MM-dd 转换为日期格式
	 * 
	 * @param strDate
	 * @return
	 */
	public static final Date strToDate(String strDate) {
		if (strToSimpleFormat(strDate) != null) {
			return strToSimpleFormat(strDate);
		} else {
			return strToDtSimpleFormat(strDate);
		}

	}

	/**
	 * 获取输入日期的相差日期
	 * 
	 * @param dt
	 * @param idiff
	 * 
	 * @return
	 */
	public static final Date getDiffDate(Date dt, int idiff) {
		Calendar c = Calendar.getInstance();

		c.setTime(dt);
		c.add(Calendar.DATE, idiff);
		return c.getTime();
	}

	public static final String getDiffStr(Date dt, int idiff) {
		Calendar c = Calendar.getInstance();

		c.setTime(dt);
		c.add(Calendar.DATE, idiff);
		return dtSimpleFormat(c.getTime());
	}

	/**
	 * 获取输入日期的相差日期
	 * 
	 * @param dt
	 * @param idiff
	 * 
	 * @return
	 */
	public static final String getDiffhour(Date dt, int idiff) {
		Calendar c = Calendar.getInstance();

		c.setTime(dt);
		c.add(Calendar.HOUR, idiff);
		return dtSimpleFormat(c.getTime());
	}

	public static final String getDiffhour(int idiff) {
		return getDiffhour(new Date(), idiff);
	}

	/**
	 * 获取输入日期的相差日期
	 * 
	 * @param dt
	 * @param idiff
	 * 
	 * @return
	 */
	public static final Date getDiffDateTime(Date dt, int idiff) {
		Calendar c = Calendar.getInstance();

		c.setTime(dt);
		c.add(Calendar.DATE, idiff);
		return c.getTime();
	}

	/**
	 * 获取输入日期月份的相差日期
	 * 
	 * @param dt
	 * @param idiff
	 * @return
	 */
	public static final String getDiffMon(Date dt, int idiff) {
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.MONTH, idiff);
		return dtSimpleFormat(c.getTime());
	}

	/**
	 * yyyy年MM月dd日
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static final String dtSimpleChineseFormat(Date date) {
		if (date == null) {
			return "";
		}

		return getFormat(DT_SIMPLE_CHINESE).format(date);
	}

	/**
	 * yyyy-MM-dd到 yyyy年MM月dd日 转换
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static final String dtSimpleChineseFormatStr(String date) throws ParseException {
		if (date == null) {
			return "";
		}

		return getFormat(DT_SIMPLE_CHINESE).format(string2Date(date));
	}

	/**
	 * yyyy-MM-dd 日期字符转换为时间
	 * 
	 * @param stringDate
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static final Date string2Date(String stringDate) throws ParseException {
		if (stringDate == null) {
			return null;
		}

		return getFormat(DT_SIMPLE).parse(stringDate);
	}

	/**
	 * 返回日期时间（Add by Sunzy）
	 * 
	 * @param stringDate
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static final Date string2DateTime(String stringDate) throws ParseException {
		if (stringDate == null) {
			return null;
		}

		return getFormat(SIMPLE).parse(stringDate);
	}

	/**
	 * 返回日期时间（Add by Sunzy）
	 * 
	 * @param stringDate
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static final Date string2DateTimeByAutoZero(String stringDate) throws ParseException {
		if (stringDate == null) {
			return null;
		}
		if (stringDate.length() == 11)
			stringDate = stringDate + "00:00:00";
		else if (stringDate.length() == 13)
			stringDate = stringDate + ":00:00";
		else if (stringDate.length() == 16)
			stringDate = stringDate + ":00";
		else if (stringDate.length() == 10)
			stringDate = stringDate + " 00:00:00";

		return getFormat(SIMPLE).parse(stringDate);
	}

	/**
	 * 计算日期差值
	 * 
	 * @param String
	 * @param String
	 * @return int（天数）
	 */
	public static final int calculateDecreaseDate(String beforDate, String afterDate) throws ParseException {
		Date date1 = getFormat(DT_SIMPLE).parse(beforDate);
		Date date2 = getFormat(DT_SIMPLE).parse(afterDate);
		long decrease = getDateBetween(date1, date2) / 1000 / 3600 / 24;
		int dateDiff = (int) decrease;
		return dateDiff;
	}

	/**
	 * 计算时间差
	 * 
	 * @param dBefor
	 *            首日
	 * @param dAfter
	 *            尾日
	 * @return 时间差(毫秒)
	 */
	public static final long getDateBetween(Date dBefor, Date dAfter) {
		long lBefor = 0;
		long lAfter = 0;
		long lRtn = 0;

		/** 取得距离 1970年1月1日 00:00:00 GMT 的毫秒数 */
		lBefor = dBefor.getTime();
		lAfter = dAfter.getTime();

		lRtn = lAfter - lBefor;

		return lRtn;
	}

	/**
	 * 返回日期时间（Add by Gonglei）
	 * 
	 * @param stringDate
	 *            (yyyyMMdd)
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static final Date shortstring2Date(String stringDate) throws ParseException {
		if (stringDate == null) {
			return null;
		}

		return getFormat(DT_SHORT).parse(stringDate);
	}

	/**
	 * yymmdd 转 Date
	 * 
	 * @param stringDate
	 * @return
	 * @throws ParseException
	 */
	public static final Date yymmdd2Date(String stringDate) throws ParseException {
		if (stringDate == null) {
			return null;
		}
		return getFormat(SIMPLE).parse(stringDate);
	}

	/**
	 * 返回短日期格式（yyyyMMdd格式）
	 * 
	 * @param stringDate
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static final String shortDate(Date Date) {
		if (Date == null) {
			return null;
		}

		return getFormat(DT_SHORT).format(Date);
	}

	/**
	 * 返回长日期格式（yyyyMMddHHmmss格式）
	 * 
	 * @param stringDate
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static final String longDate(Date Date) {
		if (Date == null) {
			return null;
		}
		return getFormat(DT_LONG).format(Date);
	}

	/**
	 * 返回12位日期，年月日时分 ，没有秒
	 * 
	 * @param date
	 * @return
	 */
	public static final String getTwelveBitDate(Date date) {
		if (date == null) {
			return null;
		}
		return getFormat(DT_SIXTEEN_BIT).format(date);
	}

	/**
	 * yyyy-MM-dd 日期字符转换为长整形
	 * 
	 * @param stringDate
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static final Long string2DateLong(String stringDate) throws ParseException {
		Date d = string2Date(stringDate);

		if (d == null) {
			return null;
		}

		return Long.valueOf(d.getTime());
	}

	/**
	 * yyyy-MM-dd 日期字符转换为长整形
	 * 
	 * @param stringDate
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static final Long string2DateLong(String format, String stringDate) {
		Date d = string2Date(format, stringDate);

		if (d == null) {
			return null;
		}

		return Long.valueOf(d.getTime());
	}

	/**
	 * 日期转换为字符串 HH:mm:ss
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static final String hmsFormat(Date date) {
		if (date == null) {
			return "";
		}

		return getFormat(HMS_FORMAT).format(date);
	}

	/**
	 * 时间转换字符串 2005-06-30 15:50
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static final String simpleDate(Date date) {
		if (date == null) {
			return "";
		}

		return getFormat(SIMPLE_FORMAT).format(date);
	}

	/**
	 * 获取当前日期的日期差 now= 2005-07-19 diff = 1 -> 2005-07-20 diff = -1 -> 2005-07-18
	 * 
	 * @param diff
	 * 
	 * @return
	 */
	public static final Date getDiffDate(int diff) {
		Calendar c = Calendar.getInstance();

		c.setTime(new Date());
		c.add(Calendar.DATE, diff);
		return c.getTime();
	}

	public static final Date getDiffDateHour(int diff) {
		Calendar c = Calendar.getInstance();

		c.setTime(new Date());
		c.add(Calendar.HOUR, diff);
		return c.getTime();
	}

	/**
	 * 获取当前日期的日期差 now= 2005-07-19 diff = 1 -> 2005-07-20 diff = -1 -> 2005-07-18
	 * 
	 * @param diff
	 * 
	 * @return
	 */
	public static final String getDiffStr(int diff) {
		Calendar c = Calendar.getInstance();

		c.setTime(new Date());
		c.add(Calendar.DATE, diff);
		return dtSimpleFormat(c.getTime());
	}

	/**
	 * 获取当前日
	 * 
	 * @return 参数为输出的日期格式：yyyyMMdd
	 */
	public static String getNowDate(String format) {
		Date dt = new Date();
		SimpleDateFormat matter1 = new SimpleDateFormat(format);
		return matter1.format(dt);

	}

	public static final Date getDiffDateTime(int diff) {
		Calendar c = Calendar.getInstance();

		c.setTime(new Date());
		c.add(Calendar.DATE, diff);
		return c.getTime();
	}

	/**
	 * 获取当前日期的日期时间差
	 * 
	 * @param diff
	 * @param hours
	 * 
	 * @return
	 */
	public static final String getDiffDateTime(int diff, int hours) {
		Calendar c = Calendar.getInstance();

		c.setTime(new Date());
		c.add(Calendar.DATE, diff);
		c.add(Calendar.HOUR, hours);
		return dtSimpleFormat(c.getTime());
	}

	public static final String getDiffDate(String srcDate, String format, int diff) {
		DateFormat f = new SimpleDateFormat(format);

		try {
			Date source = f.parse(srcDate);
			Calendar c = Calendar.getInstance();

			c.setTime(source);
			c.add(Calendar.DATE, diff);
			return f.format(c.getTime());
		} catch (Exception e) {
			return srcDate;
		}
	}

	/**
	 * 把日期类型的日期换成数字类型 YYYYMMDD类型
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static final Long dateToNumber(Date date) {
		if (date == null) {
			return null;
		}

		Calendar c = Calendar.getInstance();

		c.setTime(date);

		String month;
		String day;

		if ((c.get(Calendar.MONTH) + 1) >= 10) {
			month = "" + (c.get(Calendar.MONTH) + 1);
		} else {
			month = "0" + (c.get(Calendar.MONTH) + 1);
		}

		if (c.get(Calendar.DATE) >= 10) {
			day = "" + c.get(Calendar.DATE);
		} else {
			day = "0" + c.get(Calendar.DATE);
		}

		String number = c.get(Calendar.YEAR) + "" + month + day;

		return new Long(number);
	}

	/**
	 * 获取每月的某天到月末的区间
	 * 
	 * @param date
	 * 
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map getLastWeek(String StringDate, int interval) throws ParseException {
		Map lastWeek = new HashMap();
		Date tempDate = DateUtil.shortstring2Date(StringDate);
		Calendar cad = Calendar.getInstance();

		cad.setTime(tempDate);

		int dayOfMonth = cad.getActualMaximum(Calendar.DAY_OF_MONTH);

		cad.add(Calendar.DATE, (dayOfMonth - 1));
		lastWeek.put("endDate", DateUtil.shortDate(cad.getTime()));
		cad.add(Calendar.DATE, interval);
		lastWeek.put("startDate", DateUtil.shortDate(cad.getTime()));

		return lastWeek;
	}

	/**
	 * 获取下月
	 * 
	 * @param date
	 * 
	 * @return
	 */
	public static String getNextMon(String StringDate) throws ParseException {
		Date tempDate = DateUtil.shortstring2Date(StringDate);
		Calendar cad = Calendar.getInstance();

		cad.setTime(tempDate);
		cad.add(Calendar.MONTH, 1);
		return DateUtil.shortDate(cad.getTime());
	}

	/**
	 * add by daizhixia 20050808 获取下一天
	 * 
	 * @param StringDate
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static String getNextDay(String StringDate) throws ParseException {
		Date tempDate = DateUtil.string2Date(StringDate);
		Calendar cad = Calendar.getInstance();

		cad.setTime(tempDate);
		cad.add(Calendar.DATE, 1);
		return DateUtil.dtSimpleFormat(cad.getTime());
	}

	/**
	 * add by chencg 获取下一天 返回 dtSimple 格式字符
	 * 
	 * @param date
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static String getNextDay(Date date) throws ParseException {
		Calendar cad = Calendar.getInstance();
		cad.setTime(date);
		cad.add(Calendar.DATE, 1);
		return DateUtil.dtSimpleFormat(cad.getTime());
	}

	public static Date getNextDayDate(Date date) throws ParseException {
		Calendar cad = Calendar.getInstance();
		cad.setTime(date);
		cad.add(Calendar.DATE, 1);
		return cad.getTime();
	}

	/**
	 * add by shengyong 20050808 获取前一天
	 * 
	 * @param StringDate
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static String getBeforeDay(String StringDate) throws ParseException {
		Date tempDate = DateUtil.string2Date(StringDate);
		Calendar cad = Calendar.getInstance();

		cad.setTime(tempDate);
		cad.add(Calendar.DATE, -1);
		return DateUtil.dtSimpleFormat(cad.getTime());
	}

	/**
	 * add by shengyong 获取前一天 返回 dtSimple 格式字符
	 * 
	 * @param date
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static String getBeforeDay(Date date) {
		Calendar cad = Calendar.getInstance();
		cad.setTime(date);
		cad.add(Calendar.DATE, -1);
		return DateUtil.dtSimpleFormat(cad.getTime());
	}

	/**
	 * 获取昨天日期 格式：yyyyMMdd
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static String getYesterday(Date date) throws ParseException {
		Calendar cad = Calendar.getInstance();
		cad.setTime(date);
		cad.add(Calendar.DATE, -1);
		return DateUtil.shortDate(cad.getTime());

	}

	/**
	 * 获取昨天日期 格式：yyyy-MM-dd 2016年3月4日 下午1:53:42 Louis Lau
	 * 
	 * @param date
	 * @return Date
	 */
	public static Date getYesterDateDtSimple(Date date) {

		Calendar cad = Calendar.getInstance();
		cad.setTime(date);
		cad.add(Calendar.DATE, -1);
		return cad.getTime();

	}

	/**
	 * 获取昨天日期 格式：yyyy-MM-dd
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static String getYesterDate(Date date) throws ParseException {
		Calendar cad = Calendar.getInstance();
		cad.setTime(date);
		cad.add(Calendar.DATE, -1);
		return DateUtil.dtSimpleFormat(cad.getTime());

	}

	public static Date getYesterDayDate(Date date) throws ParseException {
		Calendar cad = Calendar.getInstance();
		cad.setTime(date);
		cad.add(Calendar.DATE, -1);
		return cad.getTime();

	}

	/**
	 * add by chencg 获取下一天 返回 dtshort 格式字符
	 * 
	 * @param StringDate
	 *            "20061106"
	 * 
	 * @return String "2006-11-07"
	 * 
	 * @throws ParseException
	 */
	public static Date getNextDayDtShort(String StringDate) throws ParseException {
		Date tempDate = DateUtil.shortstring2Date(StringDate);
		Calendar cad = Calendar.getInstance();

		cad.setTime(tempDate);
		cad.add(Calendar.DATE, 1);
		return cad.getTime();
	}

	/**
	 * add by daizhixia 20050808 取得相差的天数
	 * 
	 * @param startDate
	 * @param endDate
	 * 
	 * @return
	 */
	public static long countDays(String startDate, String endDate) {
		Date tempDate1 = null;
		Date tempDate2 = null;
		long days = 0;
		try {
			tempDate1 = DateUtil.string2Date(startDate);
			tempDate2 = DateUtil.string2Date(endDate);
			days = (tempDate2.getTime() - tempDate1.getTime()) / (1000 * 60 * 60 * 24);
		} catch (ParseException e) {
			LOGGER.error("日期处理错误", e);
		}
		return days;
	}

	/**
	 * 返回日期相差天数，向下取整数
	 * 
	 * @param dateStart
	 *            一般前者小于后者dateEnd
	 * @param dateEnd
	 * 
	 * @return
	 */
	public static int countDays(Date dateStart, Date dateEnd) {
		if ((dateStart == null) || (dateEnd == null)) {
			return -1;
		}

		return (int) ((dateEnd.getTime() - dateStart.getTime()) / (1000 * 60 * 60 * 24));
	}

	/**
	 * 校验start与end相差的天数，是否满足end-start lessEqual than days
	 * 
	 * @param start
	 * @param end
	 * @param days
	 * 
	 * @return
	 */
	public static boolean checkDays(Date start, Date end, int days) {
		int g = countDays(start, end);

		return g <= days;
	}

	public static Date now() {
		return new Date();
	}

	/**
	 * alahan add 20050825 获取传入时间相差的日期
	 * 
	 * @param dt
	 *            传入日期，可以为空
	 * @param diff
	 *            需要获取相隔diff天的日期 如果为正则取以后的日期，否则时间往前推
	 * 
	 * @return
	 */
	public static String getDiffStringDate(Date dt, int diff) {
		Calendar ca = Calendar.getInstance();

		if (dt == null) {
			ca.setTime(new Date());
		} else {
			ca.setTime(dt);
		}

		ca.add(Calendar.DATE, diff);
		return dtSimpleFormat(ca.getTime());
	}

	/**
	 * 校验输入的时间格式是否合法，但不需要校验时间一定要是8位的
	 * 
	 * @param statTime
	 * 
	 * @return alahan add 20050901
	 */
	public static boolean checkTime(String statTime) {
		if (statTime.length() > 8) {
			return false;
		}

		String[] timeArray = statTime.split(":");

		if (timeArray.length != 3) {
			return false;
		}

		for (int i = 0; i < timeArray.length; i++) {
			String tmpStr = timeArray[i];

			try {
				Integer tmpInt = new Integer(tmpStr);

				if (i == 0) {
					if ((tmpInt.intValue() > 23) || (tmpInt.intValue() < 0)) {
						return false;
					} else {
						continue;
					}
				}

				if ((tmpInt.intValue() > 59) || (tmpInt.intValue() < 0)) {
					return false;
				}
			} catch (Exception e) {
				return false;
			}
		}

		return true;
	}

	/**
	 * 返回日期时间（Add by Gonglei）
	 * 
	 * @param stringDate
	 *            (yyyyMMdd)
	 * 
	 * @return
	 * 
	 * @throws ParseException
	 */
	public static final String stringToStringDate(String stringDate) {
		if (stringDate == null) {
			return null;
		}

		if (stringDate.length() != 8) {
			return null;
		}

		return stringDate.substring(0, 4) + stringDate.substring(4, 6) + stringDate.substring(6, 8);
	}

	/**
	 * 将字符串按format格式转换为date类型
	 * 
	 * @param str
	 * @param format
	 * 
	 * @return
	 */
	public static Date string2Date(String str, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		try {
			return sdf.parse(str);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 加减天数
	 * 
	 * @param date
	 * @return Date
	 * @author shencb 2006-12 add
	 */
	public static final Date increaseDate(Date aDate, int days) {
		Calendar cal = Calendar.getInstance();

		cal.setTime(aDate);
		cal.add(Calendar.DAY_OF_MONTH, days);
		return cal.getTime();
	}

	/**
	 * 把日期2007/06/14转换为20070614
	 * 
	 * @author Yufeng 2007
	 * @method formatDateString
	 * @param date
	 * @return
	 */
	public static String formatDateString(String date) {
		String result = "";
		if (StringUtils.isBlank(date)) {
			return "";
		}
		if (date.length() == 10) {
			result = date.substring(0, 4) + date.substring(5, 7) + date.substring(8, 10);
		}
		return result;
	}

	/**
	 * 获得日期是周几
	 * 
	 * @author xiang.zhaox
	 * @param date
	 * @return dayOfWeek
	 */
	public static int getDayOfWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * 将8位日期转换为10位日期（Add by Alcor）
	 * 
	 * @param stringDate
	 *            yyyymmdd
	 * @return yyyy-mm-dd
	 * @throws ParseException
	 */
	public static final String shortString2SimpleString(String shortString) {
		if (shortString == null) {
			return null;
		}
		try {
			return getFormat(DT_SIMPLE).format(shortstring2Date(shortString));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * 获取当前时间的8位字符串：yyyyMMdd
	 * 
	 * @return yyyyMMdd
	 * 
	 */
	public static String getDateToSimpleString() {
		Date now = new Date();
		DateFormat dateFormat = new SimpleDateFormat(DT_SHORT);
		String nowStr = dateFormat.format(now);
		return nowStr;
	}

	public static String getYesterdayByMd_Format() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, -1);
		String yesterStr = getFormat(MD_FORMAT).format(c.getTime());
		return yesterStr;
	}

	/**
	 * 比较两个时间是否超过 10分钟
	 * 
	 * date1 - date2 >= 10分钟 ，return false date1 - date2 < 10分钟 ，return true
	 * 
	 * @param date1
	 *            现在的时间 yyyy-MM-dd HH:mm:ss
	 * @param date2
	 *            以前的时间 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static boolean compareDate(Date date1, Date date2) {
		long gap = date1.getTime() - date2.getTime();
		if (gap >= MILLISECOND) {
			return false;
		}
		return true;
	}

	/**
	 * 输入开始时间，返回 执行时间
	 * 
	 * @param startTime
	 * @return
	 */
	public static String requestTime(long startTime) {
		return Long.toString(System.currentTimeMillis() - startTime);
	}

	/**
	 * 计算两个日期之间相差的天数
	 * 
	 * @param minDate
	 * @param maxDate
	 * @return
	 */
	public static int daysBetween(Date minDate, Date maxDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(minDate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(maxDate);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * 获取下月相应日期
	 * 
	 * 2016-01-01 -->2016-02-01 2016-01-31 -->2016-02-29 2016-12-31 -->2017-01-31
	 * 
	 * 2016年3月4日 上午10:49:01 Louis Lau
	 * 
	 * @param date
	 *            格式：yyyy-MM-dd
	 * @return
	 */
	public static Date getDateOfNextMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, +1);
		return c.getTime();
	}

	/**
	 * 获取上月相应日期
	 * 
	 * 2016-03-31-->2016-02-29 2016-03-30-->2016-02-29 2016-03-29-->2016-02-29
	 * 2016-01-01-->2015-12-01
	 * 
	 * 2016年3月5日 下午7:21:17 Louis Lau
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDateOfLastMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, -1);
		return c.getTime();
	}

	/**
	 * 格式化日期 yyyy-MM-dd 2016年2月29日 下午1:38:03 Louis Lau
	 * 
	 * @param date
	 * @return
	 */
	public static Date formatNewDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DT_SIMPLE);
		try {
			return dateFormat.parse(dateFormat.format(date));
		} catch (ParseException e) {
			throw new RuntimeException("日期转换发生异常", e);
		}
	}

	public static boolean isLastDayofMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		Calendar newCalendar = Calendar.getInstance();
		newCalendar.set(Calendar.MONTH, c.get(Calendar.MONTH));
		newCalendar.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date lastDay = formatNewDate(newCalendar.getTime());
		if (date.equals(lastDay)) {
			return true;
		}
		return false;
	}

	public static Date getLastDayOfNextMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, 1);

		Calendar newCalendar = Calendar.getInstance();
		newCalendar.set(Calendar.YEAR, c.get(Calendar.YEAR));
		newCalendar.set(Calendar.MONTH, c.get(Calendar.MONTH));
		newCalendar.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		return formatNewDate(newCalendar.getTime());
	}

	public static long getDaysBetweenDate(String dateStr) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		if (StringUtils.isEmpty(dateStr)) {
			return 0L;
		}
		Date date = sdf.parse(dateStr);
		Date nowDate = new Date();

		Calendar cal_1 = Calendar.getInstance();
		Calendar cal_2 = Calendar.getInstance();

		cal_1.setTime(date);
		cal_2.setTime(nowDate);

		return (cal_2.getTimeInMillis() - cal_1.getTimeInMillis()) / (1000 * 3600 * 24);
	}

	public static long getDaysBetweenDate(String dateStr, Date nowDate) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		if (StringUtils.isEmpty(dateStr)) {
			return 0L;
		}
		Date date = sdf.parse(dateStr);

		Calendar cal_1 = Calendar.getInstance();
		Calendar cal_2 = Calendar.getInstance();

		cal_1.setTime(date);
		cal_2.setTime(nowDate);

		return (cal_2.getTimeInMillis() - cal_1.getTimeInMillis()) / (1000 * 3600 * 24);
	}

	public static String getCurrentSeason() {
		Calendar calendar = Calendar.getInstance();
		int currentYear = calendar.get(Calendar.YEAR);
		int currentMonth = calendar.get(Calendar.MONTH) + 1;
		if (currentMonth >= 1 && currentMonth <= 3) {
			return currentYear + "年第1季度";
		} else if (currentMonth >= 4 && currentMonth <= 6) {
			return currentYear + "年第2季度";
		} else if (currentMonth >= 7 && currentMonth <= 9) {
			return currentYear + "年第3季度";
		} else {
			return currentYear + "年第4季度";
		}
	}

	public static List<String> getCurrentSeasonMonth() {
		List<String> ret = new ArrayList<String>(5);
		Calendar calendar = Calendar.getInstance();
		int currentYear = calendar.get(Calendar.YEAR);
		int currentMonth = calendar.get(Calendar.MONTH) + 1;
		if (currentMonth >= 1 && currentMonth <= 3) {
			ret.add(currentYear + "-01");
			ret.add(currentYear + "-02");
			ret.add(currentYear + "-03");
		} else if (currentMonth >= 4 && currentMonth <= 6) {
			ret.add(currentYear + "-04");
			ret.add(currentYear + "-05");
			ret.add(currentYear + "-06");
		} else if (currentMonth >= 7 && currentMonth <= 9) {
			ret.add(currentYear + "-07");
			ret.add(currentYear + "-08");
			ret.add(currentYear + "-09");
		} else {
			ret.add(currentYear + "-10");
			ret.add(currentYear + "-11");
			ret.add(currentYear + "-12");
		}
		return ret;
	}

	public static int getLeftDaysOfMonth() {
		// 获取当前时间
		Calendar cal = Calendar.getInstance();

		int curDay = cal.get(Calendar.DAY_OF_MONTH);
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		return maxDay - curDay;
	}

	public static int getLeftDaysOfSeason() {
		// 获取当前时间
		Calendar cal = Calendar.getInstance();

		int leftDays = 0;
		int lastMonth = 0;
		int month = cal.get(Calendar.MONTH);
		if (month <= 2) {
			lastMonth = 2;
		} else if (month <= 5) {
			lastMonth = 5;
		} else if (month <= 8) {
			lastMonth = 8;
		} else {
			lastMonth = 11;
		}
		int temp = lastMonth - month;

		int curDay = cal.get(Calendar.DAY_OF_MONTH);
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		if (temp == 0) {

			leftDays = maxDay - curDay;

		} else if (temp == 1) {

			cal.set(Calendar.MONTH, month + 1);
			int tempMax = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			leftDays = maxDay - curDay + tempMax;

		} else if (temp == 2) {

			cal.set(Calendar.MONTH, month + 1);
			int tempMax1 = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

			cal.set(Calendar.MONTH, month + 2);
			int tempMax2 = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

			leftDays = maxDay - curDay + tempMax1 + tempMax2;
		}

		return leftDays;
	}

	/**
	 * 两个日期的相差天数
	 * 
	 * @Title: differDays
	 * @Description: TODO
	 * @param endDate
	 * @param startDate
	 * @return
	 * @return: int
	 */
	public static long getDifferDays(Date endDate, Date startDate) {

		return (endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);
	}

	/**
	 * 获取该日期的前n个月的相同日期
	 * 
	 * @Title: getMonthForCount
	 * @Description: TODO
	 * @param i
	 * @param date
	 * @return
	 * @return: Date
	 */
	public Date getMonthForCount(int i, Date date) {
		for (int j = 0; j < i; j++) {
			date = DateUtil.getDateOfLastMonth(date);
		}
		return date;
	}

	/**
	 * 
	 * @Title: getLastMonth
	 * @Description: TODO
	 * @param date
	 * @return yyyyMMdd
	 * @return: String
	 */
	public static String getLastMonth(String dateStr, String fomat) {
		try {
			Date d = getFormat(DT_SHORT).parse(dateStr);
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			c.add(Calendar.MONTH, -1);
			return getFormat(DT_YYYYMM).format(c.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;

	}

	public static Date stringFormatDate(String strDate, String dateFormat) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			Date date = sdf.parse(strDate);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
