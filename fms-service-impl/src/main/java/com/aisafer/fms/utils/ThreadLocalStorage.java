package com.aisafer.fms.utils;

public class ThreadLocalStorage {
	
	public  final ThreadLocal<String> local = new ThreadLocal<>();
	
	public  String get() {
		return local.get();
	}
	
	public  void put(String value) {
		local.set(value);
	}
}
