package com.aisafer.fms.storage;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.aisafer.fms.client.dto.AuthReq;
import com.aisafer.fms.config.AttachConfig;
/**
 * @author stevin
 */
public abstract class AbstractStorage {
	
	@Autowired
	protected AttachConfig attachConfig;
	
	protected Date getExpirationDate(AuthReq req){
		if(StringUtils.isEmpty(String.valueOf(req.getUrlExpire()))){
			return new Date(System.currentTimeMillis() + attachConfig.getUrlDefalutExpire()*1000); 
		}
		return new Date(System.currentTimeMillis() + req.getUrlExpire()*1000);
		
	}
}
