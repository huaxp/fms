package com.aisafer.fms.storage;
/*package com.aisafer.oss.storage;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.aisafer.oss.config.AttachConfig;
import com.aisafer.oss.domain.Attachment;
import com.aisafer.oss.domain.AttachmentE;
import com.aisafer.oss.domain.Module;
import com.aisafer.oss.domain.StsConfig;
import com.aisafer.oss.dto.AuthReq;
import com.aisafer.oss.dto.DownReq;
import com.aisafer.oss.enums.AttachmentStatusEnums;
import com.aisafer.oss.enums.StorageTypeEnums;
import com.aisafer.oss.exception.AliyunException;
import com.aisafer.oss.exception.StorageServiceException;
import com.aisafer.oss.mapper.AttachmentEMapper;
import com.aisafer.oss.mapper.AttachmentMapper;
import com.aisafer.oss.mapper.ModuleMapper;
import com.aisafer.oss.mapper.StsConfigMapper;
import com.aisafer.oss.service.IStorageManager;
import com.aisafer.oss.service.impl.AttachmentService;
import com.aisafer.oss.service.impl.OSSPool;
import com.aisafer.oss.service.impl.PoolClient;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.ServiceException;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.FetchRet;
import com.qiniu.util.Auth;

*//**
 * 七云
 *
 * @author stevin
 *//*
@Service("QCLOUD")
public class Qcloud extends AbstractStorage implements IStorageManager {

	private static final Logger log = LoggerFactory.getLogger(Qcloud.class);

	@Autowired
	private StsConfigMapper stsConfigDao;

	@Autowired
	private AttachConfig attachConfig;

	@Autowired
	ModuleMapper moduleMapper;

	@Autowired
	private AttachmentService attachmentService;

	@Autowired
	protected AttachmentEMapper attachmentEDao;

	@Autowired
	protected AttachmentMapper attachmentDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void upload(AttachmentE attachmentE, byte[] content) throws StorageServiceException {
		PoolClient<OSSClient> poolClient = null;
		StsConfig stsConfig = null;
		try {
			log.info("qcloudPath:{},attachid:{}", attachmentE.getStorageKey(), attachmentE.getAttachid());
			Attachment attachment = attachmentDao.selectByPrimaryKey(attachmentE.getAttachid());
			if (attachment == null) {
				log.error("attach {} is empty.", attachmentE.getAttachid());
				return;
			}
			Module moduleObj = moduleMapper.selectByPrimaryKey(attachment.getModuleId());
			StsConfig sts = new StsConfig();
			sts.setModuleId(moduleObj.getId());
			sts.setCloudStorageMode(StorageTypeEnums.QCLOUD.name());
			stsConfig = stsConfigDao.select(sts);
			if (stsConfig == null || stsConfig.getId() <= 0) {
				log.error("sts 配置为空 id {} ", moduleObj.getId());
				throw new ServiceException("sts 配置为空");
			}

			Zone z = Zone.autoZone();
			Configuration c = new Configuration(z);
			// 创建上传对象
			UploadManager uploadManager = new UploadManager(c);
			final String filePath = attachmentE.getStorageKey().substring(0,
					attachmentE.getStorageKey().lastIndexOf("/"));
			final String key = attachmentE.getStorageKey().substring(attachmentE.getStorageKey().lastIndexOf("/"),
					attachmentE.getStorageKey().length());
			Auth auth = Auth.create(sts.getAccessKeyId(), sts.getAccessKeySecret());
			Response res = uploadManager.put(filePath, key, getUpToken(auth, stsConfig));
			attachmentService.saveAttach(attachmentE, StorageTypeEnums.QCLOUD, AttachmentStatusEnums.SUCCESS);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new AliyunException(e.getMessage());
		} finally {
			if (poolClient != null) {
				OSSPool.getOssPool(attachConfig).shutdown(poolClient);
			}
		}
	}

	private String getUpToken(Auth auth, StsConfig sts) {
		return auth.uploadToken(sts.getBucketName());
	}

	@Override
	public byte[] read(AttachmentE atte) {
		try {
			Attachment attachment = attachmentDao.selectByPrimaryKey(atte.getAttachid());
			if (attachment == null) {
				return null;
			}
			Module moduleObj = moduleMapper.selectByPrimaryKey(attachment.getModuleId());
			StsConfig sts = new StsConfig();
			sts.setModuleId(moduleObj.getId());
			StsConfig stsConfig = stsConfigDao.select(sts);
			if (stsConfig == null || stsConfig.getId() <= 0) {
				log.error("sts 配置为空 id {} ", moduleObj.getId());
				throw new ServiceException("sts 配置为空");
			}
			Auth auth = Auth.create(sts.getAccessKeyId(), sts.getAccessKeySecret());

			Zone z = Zone.zone0();
			Configuration c = new Configuration(z);

			// 实例化一个BucketManager对象
			BucketManager bucketManager = new BucketManager(auth, c);

			try {
				// 调用fetch方法抓取文件
				FetchRet fet = bucketManager.fetch(sts.getAccessWanUrl(), sts.getBucketName(), atte.getStorageKey());

			} catch (QiniuException e) {
				// 捕获异常信息
				Response r = e.response;
				log.error(e.getMessage());
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;
		}
		return null;
	}

	@Override
	public String getAuthUrl(AuthReq req) throws ServiceException {

		return null;
	}

	private String getGennerateUrl(Module module, String storageKey, AuthReq req) {
		return null;
	}


	@Override
	public byte[] read(DownReq req) {

		try {
			List<AttachmentE> attaEList = attachmentService.getAttachment(req.getAttachid(),
					StorageTypeEnums.ALIYUN.name());
			if (attaEList != null && attaEList.size() > 0) {
				AttachmentE attachmentE = attaEList.get(0);
				Attachment attachment = attachmentDao.selectByPrimaryKey(attachmentE.getAttachid());
				Module moduleObj = moduleMapper.selectByPrimaryKey(attachment.getModuleId());
				StsConfig sts = new StsConfig();
				sts.setModuleId(moduleObj.getId());
				StsConfig stsConfig = stsConfigDao.select(sts);
				if (stsConfig == null || stsConfig.getId() <= 0) {
					log.error("sts 配置为空 id {} ", moduleObj.getId());
					throw new ServiceException("sts 配置为空");
				}
				return getOssObject(stsConfig, req);
			} else if (StringUtils.isNotEmpty(req.getModulekey())) {
				Module module = new Module();
				module.setModuleKey(req.getModulekey());
				Module moduleObj = moduleMapper.select(module);
				StsConfig sts = new StsConfig();
				sts.setModuleId(moduleObj.getId());
				StsConfig stsConfig = stsConfigDao.select(sts);
				return getOssObject(stsConfig, req);
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
		return null;
	}

	private byte[] getOssObject(StsConfig sts, DownReq req) {
		Auth auth = Auth.create(sts.getAccessKeyId(), sts.getAccessKeySecret());

		Zone z = Zone.zone0();
		Configuration c = new Configuration(z);

		// 实例化一个BucketManager对象
		BucketManager bucketManager = new BucketManager(auth, c);

		try {
			// 调用fetch方法抓取文件
			FetchRet fet = bucketManager.fetch(sts.getAccessWanUrl(), sts.getBucketName(), String.valueOf(req.getAttachid()));
			return null;
		} catch (QiniuException e) {
			// 捕获异常信息
			Response r = e.response;
			log.error(e.getMessage());
		}
		return null;
	}

}
*/