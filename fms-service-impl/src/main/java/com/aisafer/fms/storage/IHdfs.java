package com.aisafer.fms.storage;

import java.io.InputStream;

/**
 * 存储接口
 * @author stevin
 *
 */
public interface IHdfs {
	
	/**
	 * 修改
	 * @param dst
	 * @param contents
	 */
	 public void upload(String dst , byte[] contents);
	 
	 /**
	  * 上传
	  * @param src
	  * @param dst
	  */
	 public void upload(String src,String dst);
	 
	 /**
	  * 重命名
	  * @param oldName
	  * @param newName
	  * @return
	  */
	 public boolean rename(String oldName,String newName);
	 /**
	  * 删除
	  * @param filePath
	  * @return
	  */
	 public boolean delete(String filePath);
	 /**
	  * 
	  * @param path
	  * @return
	  */
	 public boolean mkdir(String path);
	 
	 public byte[] readByte(String filePath);
	 
	 public InputStream read(String filePath);
}
