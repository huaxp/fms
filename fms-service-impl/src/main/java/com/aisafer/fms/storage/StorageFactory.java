package com.aisafer.fms.storage;

import com.aisafer.fms.service.IStorageManager;
import com.aisafer.fms.service.impl.ApplicationContextUtils;

/**
 * 存储工厂类
 * @author stevin
 *
 */
public class StorageFactory {
	
	public static IStorageManager getInstance(String name){
		return (IStorageManager) ApplicationContextUtils.getBean(name);
		/*if(Aliyun.class.getSimpleName().equalsIgnoreCase(name)){
			return ApplicationContextUtils.getBean(name, Aliyun.class);
		}else if(Local.class.getSimpleName().equalsIgnoreCase(name)){
			return ApplicationContextUtils.getBean(name, Local.class);
		}else if(Hdfs.class.getSimpleName().equalsIgnoreCase(name)){
			return ApplicationContextUtils.getBean(name, Hdfs.class);
		}
		throw new NotFoundException("Not found 【"+name+"】 class.");*/
	}
}
