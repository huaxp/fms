package com.aisafer.fms.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component(value="OSSAttachConfig")
@ConfigurationProperties(prefix = "fms")
public class AttachConfig {
    private String attach;
    private String networkModel;
    private String master;
    private String slave;
    private String downOrder;
    private Long urlDefalutExpire;
    private String maxFileSize;
	private String maxRequestSize;
	private String wanUrl;
    
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
	public String getNetworkModel() {
		return networkModel;
	}
	public void setNetworkModel(String networkModel) {
		this.networkModel = networkModel;
	}
	public String getMaster() {
		return master;
	}
	public void setMaster(String master) {
		this.master = master;
	}
	public String getSlave() {
		return slave;
	}
	public void setSlave(String slave) {
		this.slave = slave;
	}
	public String getDownOrder() {
		return downOrder;
	}
	public void setDownOrder(String downOrder) {
		this.downOrder = downOrder;
	}
	public Long getUrlDefalutExpire() {
		return urlDefalutExpire;
	}
	public void setUrlDefalutExpire(Long urlDefalutExpire) {
		this.urlDefalutExpire = urlDefalutExpire;
	}
	public String getMaxFileSize() {
		return maxFileSize;
	}
	public void setMaxFileSize(String maxFileSize) {
		this.maxFileSize = maxFileSize;
	}
	public String getMaxRequestSize() {
		return maxRequestSize;
	}
	public void setMaxRequestSize(String maxRequestSize) {
		this.maxRequestSize = maxRequestSize;
	}
	public String getWanUrl() {
		return wanUrl;
	}
	public void setWanUrl(String wanUrl) {
		this.wanUrl = wanUrl;
	}
	
}
